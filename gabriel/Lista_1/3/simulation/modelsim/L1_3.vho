-- Copyright (C) 2021  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 21.1.0 Build 842 10/21/2021 SJ Lite Edition"

-- DATE "06/19/2022 16:19:33"

-- 
-- Device: Altera 10M50DAF484C7G Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_TMS~	=>  Location: PIN_H2,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TCK~	=>  Location: PIN_G2,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDI~	=>  Location: PIN_L4,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDO~	=>  Location: PIN_M5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_CONFIG_SEL~	=>  Location: PIN_H10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCONFIG~	=>  Location: PIN_H9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_nSTATUS~	=>  Location: PIN_G9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_CONF_DONE~	=>  Location: PIN_F8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_TMS~~padout\ : std_logic;
SIGNAL \~ALTERA_TCK~~padout\ : std_logic;
SIGNAL \~ALTERA_TDI~~padout\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~padout\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~padout\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~padout\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~padout\ : std_logic;
SIGNAL \~ALTERA_TMS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TCK~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TDI~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	L1_3 IS
    PORT (
	A1 : IN std_logic_vector(7 DOWNTO 0);
	A2 : IN std_logic_vector(7 DOWNTO 0);
	S : BUFFER std_logic_vector(7 DOWNTO 0);
	C : BUFFER std_logic
	);
END L1_3;

-- Design Ports Information
-- S[0]	=>  Location: PIN_E6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[1]	=>  Location: PIN_F7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[2]	=>  Location: PIN_E8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[3]	=>  Location: PIN_B2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[4]	=>  Location: PIN_A5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[5]	=>  Location: PIN_A4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[6]	=>  Location: PIN_C3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[7]	=>  Location: PIN_D8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- C	=>  Location: PIN_C4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[0]	=>  Location: PIN_D9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[0]	=>  Location: PIN_C7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[1]	=>  Location: PIN_C6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[1]	=>  Location: PIN_D10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[2]	=>  Location: PIN_D6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[2]	=>  Location: PIN_D7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[3]	=>  Location: PIN_A2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[3]	=>  Location: PIN_J10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[4]	=>  Location: PIN_B3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[4]	=>  Location: PIN_B5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[5]	=>  Location: PIN_E9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[5]	=>  Location: PIN_B4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[6]	=>  Location: PIN_B1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[6]	=>  Location: PIN_D5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1[7]	=>  Location: PIN_C5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2[7]	=>  Location: PIN_A3,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF L1_3 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_A1 : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_A2 : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_S : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_C : std_logic;
SIGNAL \~QUARTUS_CREATED_ADC1~_CHSEL_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_ADC2~_CHSEL_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \~QUARTUS_CREATED_UNVM~~busy\ : std_logic;
SIGNAL \~QUARTUS_CREATED_ADC1~~eoc\ : std_logic;
SIGNAL \~QUARTUS_CREATED_ADC2~~eoc\ : std_logic;
SIGNAL \S[0]~output_o\ : std_logic;
SIGNAL \S[1]~output_o\ : std_logic;
SIGNAL \S[2]~output_o\ : std_logic;
SIGNAL \S[3]~output_o\ : std_logic;
SIGNAL \S[4]~output_o\ : std_logic;
SIGNAL \S[5]~output_o\ : std_logic;
SIGNAL \S[6]~output_o\ : std_logic;
SIGNAL \S[7]~output_o\ : std_logic;
SIGNAL \C~output_o\ : std_logic;
SIGNAL \A1[0]~input_o\ : std_logic;
SIGNAL \A2[0]~input_o\ : std_logic;
SIGNAL \MS1|S~combout\ : std_logic;
SIGNAL \A1[1]~input_o\ : std_logic;
SIGNAL \A2[1]~input_o\ : std_logic;
SIGNAL \SomadorX:1:SX|MS2|S~0_combout\ : std_logic;
SIGNAL \A1[2]~input_o\ : std_logic;
SIGNAL \A2[2]~input_o\ : std_logic;
SIGNAL \SomadorX:1:SX|D~0_combout\ : std_logic;
SIGNAL \SomadorX:2:SX|MS2|S~0_combout\ : std_logic;
SIGNAL \SomadorX:2:SX|D~0_combout\ : std_logic;
SIGNAL \A2[3]~input_o\ : std_logic;
SIGNAL \A1[3]~input_o\ : std_logic;
SIGNAL \SomadorX:3:SX|MS2|S~combout\ : std_logic;
SIGNAL \SomadorX:3:SX|D~0_combout\ : std_logic;
SIGNAL \A1[4]~input_o\ : std_logic;
SIGNAL \A2[4]~input_o\ : std_logic;
SIGNAL \SomadorX:4:SX|MS2|S~combout\ : std_logic;
SIGNAL \SomadorX:4:SX|D~0_combout\ : std_logic;
SIGNAL \A2[5]~input_o\ : std_logic;
SIGNAL \A1[5]~input_o\ : std_logic;
SIGNAL \SomadorX:5:SX|MS2|S~combout\ : std_logic;
SIGNAL \A2[6]~input_o\ : std_logic;
SIGNAL \A1[6]~input_o\ : std_logic;
SIGNAL \SomadorX:5:SX|D~0_combout\ : std_logic;
SIGNAL \SomadorX:6:SX|MS2|S~combout\ : std_logic;
SIGNAL \A1[7]~input_o\ : std_logic;
SIGNAL \SomadorX:6:SX|D~0_combout\ : std_logic;
SIGNAL \A2[7]~input_o\ : std_logic;
SIGNAL \S7|MS2|S~combout\ : std_logic;
SIGNAL \S7|D~0_combout\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_A1 <= A1;
ww_A2 <= A2;
S <= ww_S;
C <= ww_C;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\~QUARTUS_CREATED_ADC1~_CHSEL_bus\ <= (\~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\);

\~QUARTUS_CREATED_ADC2~_CHSEL_bus\ <= (\~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\);
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: LCCOMB_X44_Y41_N8
\~QUARTUS_CREATED_GND~I\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \~QUARTUS_CREATED_GND~I_combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~QUARTUS_CREATED_GND~I_combout\);

-- Location: IOOBUF_X20_Y39_N2
\S[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \MS1|S~combout\,
	devoe => ww_devoe,
	o => \S[0]~output_o\);

-- Location: IOOBUF_X24_Y39_N16
\S[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SomadorX:1:SX|MS2|S~0_combout\,
	devoe => ww_devoe,
	o => \S[1]~output_o\);

-- Location: IOOBUF_X24_Y39_N9
\S[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SomadorX:2:SX|MS2|S~0_combout\,
	devoe => ww_devoe,
	o => \S[2]~output_o\);

-- Location: IOOBUF_X22_Y39_N16
\S[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SomadorX:3:SX|MS2|S~combout\,
	devoe => ww_devoe,
	o => \S[3]~output_o\);

-- Location: IOOBUF_X31_Y39_N16
\S[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SomadorX:4:SX|MS2|S~combout\,
	devoe => ww_devoe,
	o => \S[4]~output_o\);

-- Location: IOOBUF_X31_Y39_N23
\S[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SomadorX:5:SX|MS2|S~combout\,
	devoe => ww_devoe,
	o => \S[5]~output_o\);

-- Location: IOOBUF_X20_Y39_N9
\S[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \SomadorX:6:SX|MS2|S~combout\,
	devoe => ww_devoe,
	o => \S[6]~output_o\);

-- Location: IOOBUF_X31_Y39_N2
\S[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \S7|MS2|S~combout\,
	devoe => ww_devoe,
	o => \S[7]~output_o\);

-- Location: IOOBUF_X24_Y39_N2
\C~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \S7|D~0_combout\,
	devoe => ww_devoe,
	o => \C~output_o\);

-- Location: IOIBUF_X31_Y39_N8
\A1[0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A1(0),
	o => \A1[0]~input_o\);

-- Location: IOIBUF_X34_Y39_N1
\A2[0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A2(0),
	o => \A2[0]~input_o\);

-- Location: LCCOMB_X26_Y38_N24
\MS1|S\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \MS1|S~combout\ = \A1[0]~input_o\ $ (\A2[0]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \A1[0]~input_o\,
	datad => \A2[0]~input_o\,
	combout => \MS1|S~combout\);

-- Location: IOIBUF_X29_Y39_N8
\A1[1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A1(1),
	o => \A1[1]~input_o\);

-- Location: IOIBUF_X31_Y39_N29
\A2[1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A2(1),
	o => \A2[1]~input_o\);

-- Location: LCCOMB_X26_Y38_N18
\SomadorX:1:SX|MS2|S~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \SomadorX:1:SX|MS2|S~0_combout\ = \A1[1]~input_o\ $ (\A2[1]~input_o\ $ (((\A1[0]~input_o\ & \A2[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A1[1]~input_o\,
	datab => \A2[1]~input_o\,
	datac => \A1[0]~input_o\,
	datad => \A2[0]~input_o\,
	combout => \SomadorX:1:SX|MS2|S~0_combout\);

-- Location: IOIBUF_X22_Y39_N29
\A1[2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A1(2),
	o => \A1[2]~input_o\);

-- Location: IOIBUF_X29_Y39_N15
\A2[2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A2(2),
	o => \A2[2]~input_o\);

-- Location: LCCOMB_X26_Y38_N20
\SomadorX:1:SX|D~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \SomadorX:1:SX|D~0_combout\ = (\A1[1]~input_o\ & ((\A2[1]~input_o\) # ((\A1[0]~input_o\ & \A2[0]~input_o\)))) # (!\A1[1]~input_o\ & (\A2[1]~input_o\ & (\A1[0]~input_o\ & \A2[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A1[1]~input_o\,
	datab => \A2[1]~input_o\,
	datac => \A1[0]~input_o\,
	datad => \A2[0]~input_o\,
	combout => \SomadorX:1:SX|D~0_combout\);

-- Location: LCCOMB_X26_Y38_N30
\SomadorX:2:SX|MS2|S~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \SomadorX:2:SX|MS2|S~0_combout\ = \A1[2]~input_o\ $ (\A2[2]~input_o\ $ (\SomadorX:1:SX|D~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A1[2]~input_o\,
	datac => \A2[2]~input_o\,
	datad => \SomadorX:1:SX|D~0_combout\,
	combout => \SomadorX:2:SX|MS2|S~0_combout\);

-- Location: LCCOMB_X26_Y38_N0
\SomadorX:2:SX|D~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \SomadorX:2:SX|D~0_combout\ = (\A1[2]~input_o\ & ((\A2[2]~input_o\) # (\SomadorX:1:SX|D~0_combout\))) # (!\A1[2]~input_o\ & (\A2[2]~input_o\ & \SomadorX:1:SX|D~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A1[2]~input_o\,
	datac => \A2[2]~input_o\,
	datad => \SomadorX:1:SX|D~0_combout\,
	combout => \SomadorX:2:SX|D~0_combout\);

-- Location: IOIBUF_X26_Y39_N1
\A2[3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A2(3),
	o => \A2[3]~input_o\);

-- Location: IOIBUF_X34_Y39_N8
\A1[3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A1(3),
	o => \A1[3]~input_o\);

-- Location: LCCOMB_X26_Y38_N2
\SomadorX:3:SX|MS2|S\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \SomadorX:3:SX|MS2|S~combout\ = \SomadorX:2:SX|D~0_combout\ $ (\A2[3]~input_o\ $ (\A1[3]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SomadorX:2:SX|D~0_combout\,
	datac => \A2[3]~input_o\,
	datad => \A1[3]~input_o\,
	combout => \SomadorX:3:SX|MS2|S~combout\);

-- Location: LCCOMB_X26_Y38_N28
\SomadorX:3:SX|D~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \SomadorX:3:SX|D~0_combout\ = (\SomadorX:2:SX|D~0_combout\ & ((\A2[3]~input_o\) # (\A1[3]~input_o\))) # (!\SomadorX:2:SX|D~0_combout\ & (\A2[3]~input_o\ & \A1[3]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SomadorX:2:SX|D~0_combout\,
	datac => \A2[3]~input_o\,
	datad => \A1[3]~input_o\,
	combout => \SomadorX:3:SX|D~0_combout\);

-- Location: IOIBUF_X26_Y39_N15
\A1[4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A1(4),
	o => \A1[4]~input_o\);

-- Location: IOIBUF_X26_Y39_N29
\A2[4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A2(4),
	o => \A2[4]~input_o\);

-- Location: LCCOMB_X26_Y38_N22
\SomadorX:4:SX|MS2|S\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \SomadorX:4:SX|MS2|S~combout\ = \SomadorX:3:SX|D~0_combout\ $ (\A1[4]~input_o\ $ (\A2[4]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SomadorX:3:SX|D~0_combout\,
	datac => \A1[4]~input_o\,
	datad => \A2[4]~input_o\,
	combout => \SomadorX:4:SX|MS2|S~combout\);

-- Location: LCCOMB_X26_Y38_N16
\SomadorX:4:SX|D~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \SomadorX:4:SX|D~0_combout\ = (\SomadorX:3:SX|D~0_combout\ & ((\A1[4]~input_o\) # (\A2[4]~input_o\))) # (!\SomadorX:3:SX|D~0_combout\ & (\A1[4]~input_o\ & \A2[4]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SomadorX:3:SX|D~0_combout\,
	datac => \A1[4]~input_o\,
	datad => \A2[4]~input_o\,
	combout => \SomadorX:4:SX|D~0_combout\);

-- Location: IOIBUF_X26_Y39_N22
\A2[5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A2(5),
	o => \A2[5]~input_o\);

-- Location: IOIBUF_X29_Y39_N1
\A1[5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A1(5),
	o => \A1[5]~input_o\);

-- Location: LCCOMB_X26_Y38_N26
\SomadorX:5:SX|MS2|S\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \SomadorX:5:SX|MS2|S~combout\ = \SomadorX:4:SX|D~0_combout\ $ (\A2[5]~input_o\ $ (\A1[5]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SomadorX:4:SX|D~0_combout\,
	datac => \A2[5]~input_o\,
	datad => \A1[5]~input_o\,
	combout => \SomadorX:5:SX|MS2|S~combout\);

-- Location: IOIBUF_X24_Y39_N29
\A2[6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A2(6),
	o => \A2[6]~input_o\);

-- Location: IOIBUF_X22_Y39_N22
\A1[6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A1(6),
	o => \A1[6]~input_o\);

-- Location: LCCOMB_X26_Y38_N12
\SomadorX:5:SX|D~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \SomadorX:5:SX|D~0_combout\ = (\SomadorX:4:SX|D~0_combout\ & ((\A2[5]~input_o\) # (\A1[5]~input_o\))) # (!\SomadorX:4:SX|D~0_combout\ & (\A2[5]~input_o\ & \A1[5]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SomadorX:4:SX|D~0_combout\,
	datac => \A2[5]~input_o\,
	datad => \A1[5]~input_o\,
	combout => \SomadorX:5:SX|D~0_combout\);

-- Location: LCCOMB_X26_Y38_N14
\SomadorX:6:SX|MS2|S\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \SomadorX:6:SX|MS2|S~combout\ = \A2[6]~input_o\ $ (\A1[6]~input_o\ $ (\SomadorX:5:SX|D~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A2[6]~input_o\,
	datac => \A1[6]~input_o\,
	datad => \SomadorX:5:SX|D~0_combout\,
	combout => \SomadorX:6:SX|MS2|S~combout\);

-- Location: IOIBUF_X24_Y39_N22
\A1[7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A1(7),
	o => \A1[7]~input_o\);

-- Location: LCCOMB_X26_Y38_N8
\SomadorX:6:SX|D~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \SomadorX:6:SX|D~0_combout\ = (\A2[6]~input_o\ & ((\A1[6]~input_o\) # (\SomadorX:5:SX|D~0_combout\))) # (!\A2[6]~input_o\ & (\A1[6]~input_o\ & \SomadorX:5:SX|D~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A2[6]~input_o\,
	datac => \A1[6]~input_o\,
	datad => \SomadorX:5:SX|D~0_combout\,
	combout => \SomadorX:6:SX|D~0_combout\);

-- Location: IOIBUF_X26_Y39_N8
\A2[7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A2(7),
	o => \A2[7]~input_o\);

-- Location: LCCOMB_X26_Y38_N10
\S7|MS2|S\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \S7|MS2|S~combout\ = \A1[7]~input_o\ $ (\SomadorX:6:SX|D~0_combout\ $ (\A2[7]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A1[7]~input_o\,
	datac => \SomadorX:6:SX|D~0_combout\,
	datad => \A2[7]~input_o\,
	combout => \S7|MS2|S~combout\);

-- Location: LCCOMB_X26_Y38_N4
\S7|D~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \S7|D~0_combout\ = (\A1[7]~input_o\ & ((\SomadorX:6:SX|D~0_combout\) # (\A2[7]~input_o\))) # (!\A1[7]~input_o\ & (\SomadorX:6:SX|D~0_combout\ & \A2[7]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \A1[7]~input_o\,
	datac => \SomadorX:6:SX|D~0_combout\,
	datad => \A2[7]~input_o\,
	combout => \S7|D~0_combout\);

-- Location: UNVM_X0_Y40_N40
\~QUARTUS_CREATED_UNVM~\ : fiftyfivenm_unvm
-- pragma translate_off
GENERIC MAP (
	addr_range1_end_addr => -1,
	addr_range1_offset => -1,
	addr_range2_end_addr => -1,
	addr_range2_offset => -1,
	addr_range3_offset => -1,
	is_compressed_image => "false",
	is_dual_boot => "false",
	is_eram_skip => "false",
	max_ufm_valid_addr => -1,
	max_valid_addr => -1,
	min_ufm_valid_addr => -1,
	min_valid_addr => -1,
	part_name => "quartus_created_unvm",
	reserve_block => "true")
-- pragma translate_on
PORT MAP (
	nosc_ena => \~QUARTUS_CREATED_GND~I_combout\,
	xe_ye => \~QUARTUS_CREATED_GND~I_combout\,
	se => \~QUARTUS_CREATED_GND~I_combout\,
	busy => \~QUARTUS_CREATED_UNVM~~busy\);

-- Location: ADCBLOCK_X43_Y52_N0
\~QUARTUS_CREATED_ADC1~\ : fiftyfivenm_adcblock
-- pragma translate_off
GENERIC MAP (
	analog_input_pin_mask => 0,
	clkdiv => 1,
	device_partname_fivechar_prefix => "none",
	is_this_first_or_second_adc => 1,
	prescalar => 0,
	pwd => 1,
	refsel => 0,
	reserve_block => "true",
	testbits => 66,
	tsclkdiv => 1,
	tsclksel => 0)
-- pragma translate_on
PORT MAP (
	soc => \~QUARTUS_CREATED_GND~I_combout\,
	usr_pwd => VCC,
	tsen => \~QUARTUS_CREATED_GND~I_combout\,
	chsel => \~QUARTUS_CREATED_ADC1~_CHSEL_bus\,
	eoc => \~QUARTUS_CREATED_ADC1~~eoc\);

-- Location: ADCBLOCK_X43_Y51_N0
\~QUARTUS_CREATED_ADC2~\ : fiftyfivenm_adcblock
-- pragma translate_off
GENERIC MAP (
	analog_input_pin_mask => 0,
	clkdiv => 1,
	device_partname_fivechar_prefix => "none",
	is_this_first_or_second_adc => 2,
	prescalar => 0,
	pwd => 1,
	refsel => 0,
	reserve_block => "true",
	testbits => 66,
	tsclkdiv => 1,
	tsclksel => 0)
-- pragma translate_on
PORT MAP (
	soc => \~QUARTUS_CREATED_GND~I_combout\,
	usr_pwd => VCC,
	tsen => \~QUARTUS_CREATED_GND~I_combout\,
	chsel => \~QUARTUS_CREATED_ADC2~_CHSEL_bus\,
	eoc => \~QUARTUS_CREATED_ADC2~~eoc\);

ww_S(0) <= \S[0]~output_o\;

ww_S(1) <= \S[1]~output_o\;

ww_S(2) <= \S[2]~output_o\;

ww_S(3) <= \S[3]~output_o\;

ww_S(4) <= \S[4]~output_o\;

ww_S(5) <= \S[5]~output_o\;

ww_S(6) <= \S[6]~output_o\;

ww_S(7) <= \S[7]~output_o\;

ww_C <= \C~output_o\;
END structure;



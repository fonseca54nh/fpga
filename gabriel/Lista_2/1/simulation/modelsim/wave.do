onerror {resume}
quietly virtual signal -install /l2_1 { (context /l2_1 )(D & S )} Solution
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Literal -radix unsigned /l2_1/A1
add wave -noupdate -format Literal -radix unsigned /l2_1/A2
add wave -noupdate -format Literal -radix unsigned /l2_1/A3
add wave -noupdate -radix unsigned /l2_1/Solution
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {197471 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {479952 ps}

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY L2_1 IS
END L2_1;

ARCHITECTURE BEHAVIORAL OF L2_1 IS

	SIGNAL A1 : STD_LOGIC;
	SIGNAL A2 : STD_LOGIC;
	SIGNAL A3 : STD_LOGIC;
	SIGNAL S : STD_LOGIC;
	SIGNAL D : STD_LOGIC;

	COMPONENT Somador IS PORT (
		A1 : IN STD_LOGIC;
		A2 : IN STD_LOGIC;
		A3 : IN STD_LOGIC;
		S : OUT STD_LOGIC;
		D : OUT STD_LOGIC
	);
	END COMPONENT;

BEGIN

	S1 : Somador PORT MAP (
		A1 => A1,
		A2 => A2,
		A3 => A3,
		S => S,
		D => D
	);
	
	PROCESS BEGIN
		A1 <= '0';
		A2 <= '0';
		A3 <= '0';
		WAIT FOR 20 NS;
		A1 <= '0';
		A2 <= '0';
		A3 <= '1';
		WAIT FOR 20 NS;
		A1 <= '0';
		A2 <= '1';
		A3 <= '0';
		WAIT FOR 20 NS;
		A1 <= '0';
		A2 <= '1';
		A3 <= '1';
		WAIT FOR 20 NS;
		A1 <= '1';
		A2 <= '0';
		A3 <= '0';
		WAIT FOR 20 NS;
		A1 <= '1';
		A2 <= '0';
		A3 <= '1';
		WAIT FOR 20 NS;
		A1 <= '1';
		A2 <= '1';
		A3 <= '0';
		WAIT FOR 20 NS;
		A1 <= '1';
		A2 <= '1';
		A3 <= '1';
		WAIT;
	END PROCESS;
	
END BEHAVIORAL;
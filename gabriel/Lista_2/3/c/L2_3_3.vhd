LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

PACKAGE BUS_TRAIL IS
	TYPE BUS_MATRIX IS ARRAY(6 DOWNTO 0) OF STD_LOGIC_VECTOR(15 DOWNTO 0);
END PACKAGE;

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE WORK.BUS_TRAIL.ALL;

ENTITY L2_3_3 IS PORT (
  E : IN BUS_MATRIX;
  S : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
  R : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
);
END L2_3_3;

ARCHITECTURE BEHAVIORAL OF L2_3_3 IS

BEGIN
	PROCESS(S, E) BEGIN
		CASE S IS
			WHEN "000" => R <= E(0);
			WHEN "001" => R <= E(1);
			WHEN "010" => R <= E(2);
			WHEN "011" => R <= E(3);
			WHEN "100" => R <= E(4);
			WHEN "101" => R <= E(5);
			WHEN "110" => R <= E(6);
			WHEN OTHERS => NULL;
		END CASE;
	END PROCESS;
END BEHAVIORAL;
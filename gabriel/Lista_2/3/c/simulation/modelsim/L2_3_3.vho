-- Copyright (C) 2021  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 21.1.0 Build 842 10/21/2021 SJ Lite Edition"

-- DATE "06/20/2022 00:30:42"

-- 
-- Device: Altera 10M50DAF484C7G Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_TMS~	=>  Location: PIN_H2,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TCK~	=>  Location: PIN_G2,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDI~	=>  Location: PIN_L4,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDO~	=>  Location: PIN_M5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_CONFIG_SEL~	=>  Location: PIN_H10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCONFIG~	=>  Location: PIN_H9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_nSTATUS~	=>  Location: PIN_G9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_CONF_DONE~	=>  Location: PIN_F8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_TMS~~padout\ : std_logic;
SIGNAL \~ALTERA_TCK~~padout\ : std_logic;
SIGNAL \~ALTERA_TDI~~padout\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~padout\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~padout\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~padout\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~padout\ : std_logic;
SIGNAL \~ALTERA_TMS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TCK~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TDI~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
LIBRARY WORK;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;
USE WORK.BUS_TRAIL.ALL;

ENTITY 	L2_3_3 IS
    PORT (
	E : IN WORK.BUS_TRAIL.bus_matrix;
	S : IN std_logic_vector(2 DOWNTO 0);
	R : OUT std_logic_vector(15 DOWNTO 0)
	);
END L2_3_3;

-- Design Ports Information
-- R[0]	=>  Location: PIN_U1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[1]	=>  Location: PIN_B12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[2]	=>  Location: PIN_N9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[3]	=>  Location: PIN_A9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[4]	=>  Location: PIN_V1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[5]	=>  Location: PIN_C11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[6]	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[7]	=>  Location: PIN_C8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[8]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[9]	=>  Location: PIN_F4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[10]	=>  Location: PIN_F5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[11]	=>  Location: PIN_T2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[12]	=>  Location: PIN_C10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[13]	=>  Location: PIN_J11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[14]	=>  Location: PIN_U4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[15]	=>  Location: PIN_U5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][0]	=>  Location: PIN_D1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[2]	=>  Location: PIN_A2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[0]	=>  Location: PIN_K5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[1]	=>  Location: PIN_G3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][0]	=>  Location: PIN_M1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][0]	=>  Location: PIN_L2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][0]	=>  Location: PIN_U2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][0]	=>  Location: PIN_K1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][0]	=>  Location: PIN_P1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][0]	=>  Location: PIN_D3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][1]	=>  Location: PIN_B11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][1]	=>  Location: PIN_B2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][1]	=>  Location: PIN_A8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][1]	=>  Location: PIN_C3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][1]	=>  Location: PIN_B10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][1]	=>  Location: PIN_B8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][1]	=>  Location: PIN_D13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][2]	=>  Location: PIN_K8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][2]	=>  Location: PIN_D2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][2]	=>  Location: PIN_L8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][2]	=>  Location: PIN_L1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][2]	=>  Location: PIN_C1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][2]	=>  Location: PIN_E1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][2]	=>  Location: PIN_K21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][3]	=>  Location: PIN_C2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][3]	=>  Location: PIN_A3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][3]	=>  Location: PIN_C9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][3]	=>  Location: PIN_E6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][3]	=>  Location: PIN_B1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][3]	=>  Location: PIN_D6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][3]	=>  Location: PIN_T1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][4]	=>  Location: PIN_T3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][4]	=>  Location: PIN_M3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][4]	=>  Location: PIN_J22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][4]	=>  Location: PIN_K9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][4]	=>  Location: PIN_L9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][4]	=>  Location: PIN_K2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][4]	=>  Location: PIN_F2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][5]	=>  Location: PIN_B4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][5]	=>  Location: PIN_D8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][5]	=>  Location: PIN_D7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][5]	=>  Location: PIN_A7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][5]	=>  Location: PIN_W10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][5]	=>  Location: PIN_C4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][5]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][6]	=>  Location: PIN_H11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][6]	=>  Location: PIN_V3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][6]	=>  Location: PIN_N20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][6]	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][6]	=>  Location: PIN_Y2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][6]	=>  Location: PIN_V4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][6]	=>  Location: PIN_N18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][7]	=>  Location: PIN_J10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][7]	=>  Location: PIN_J12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][7]	=>  Location: PIN_V5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][7]	=>  Location: PIN_E10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][7]	=>  Location: PIN_B14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][7]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][7]	=>  Location: PIN_N1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][8]	=>  Location: PIN_B7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][8]	=>  Location: PIN_R5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][8]	=>  Location: PIN_C7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][8]	=>  Location: PIN_W5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][8]	=>  Location: PIN_M20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][8]	=>  Location: PIN_A6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][8]	=>  Location: PIN_E22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][9]	=>  Location: PIN_N19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][9]	=>  Location: PIN_H4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][9]	=>  Location: PIN_R1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][9]	=>  Location: PIN_G4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][9]	=>  Location: PIN_H3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][9]	=>  Location: PIN_N8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][9]	=>  Location: PIN_K4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][10]	=>  Location: PIN_N2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][10]	=>  Location: PIN_R3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][10]	=>  Location: PIN_E4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][10]	=>  Location: PIN_J8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][10]	=>  Location: PIN_J3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][10]	=>  Location: PIN_J4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][10]	=>  Location: PIN_E9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][11]	=>  Location: PIN_J9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][11]	=>  Location: PIN_F3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][11]	=>  Location: PIN_M2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][11]	=>  Location: PIN_E3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][11]	=>  Location: PIN_K6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][11]	=>  Location: PIN_B3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][11]	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][12]	=>  Location: PIN_C5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][12]	=>  Location: PIN_D5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][12]	=>  Location: PIN_A12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][12]	=>  Location: PIN_A4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][12]	=>  Location: PIN_C6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][12]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][12]	=>  Location: PIN_D9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][13]	=>  Location: PIN_E8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][13]	=>  Location: PIN_A5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][13]	=>  Location: PIN_D10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][13]	=>  Location: PIN_A10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][13]	=>  Location: PIN_F7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][13]	=>  Location: PIN_B5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][13]	=>  Location: PIN_A11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][14]	=>  Location: PIN_M9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][14]	=>  Location: PIN_N5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][14]	=>  Location: PIN_P5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][14]	=>  Location: PIN_G1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][14]	=>  Location: PIN_R4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][14]	=>  Location: PIN_M8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][14]	=>  Location: PIN_M4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][15]	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][15]	=>  Location: PIN_H1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][15]	=>  Location: PIN_P4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][15]	=>  Location: PIN_U3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][15]	=>  Location: PIN_N3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][15]	=>  Location: PIN_F1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][15]	=>  Location: PIN_N4,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF L2_3_3 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_E : WORK.BUS_TRAIL.bus_matrix;
SIGNAL ww_S : std_logic_vector(2 DOWNTO 0);
SIGNAL ww_R : std_logic_vector(15 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_ADC1~_CHSEL_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_ADC2~_CHSEL_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \Mux16~0clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \~QUARTUS_CREATED_UNVM~~busy\ : std_logic;
SIGNAL \~QUARTUS_CREATED_ADC1~~eoc\ : std_logic;
SIGNAL \~QUARTUS_CREATED_ADC2~~eoc\ : std_logic;
SIGNAL \R[0]~output_o\ : std_logic;
SIGNAL \R[1]~output_o\ : std_logic;
SIGNAL \R[2]~output_o\ : std_logic;
SIGNAL \R[3]~output_o\ : std_logic;
SIGNAL \R[4]~output_o\ : std_logic;
SIGNAL \R[5]~output_o\ : std_logic;
SIGNAL \R[6]~output_o\ : std_logic;
SIGNAL \R[7]~output_o\ : std_logic;
SIGNAL \R[8]~output_o\ : std_logic;
SIGNAL \R[9]~output_o\ : std_logic;
SIGNAL \R[10]~output_o\ : std_logic;
SIGNAL \R[11]~output_o\ : std_logic;
SIGNAL \R[12]~output_o\ : std_logic;
SIGNAL \R[13]~output_o\ : std_logic;
SIGNAL \R[14]~output_o\ : std_logic;
SIGNAL \R[15]~output_o\ : std_logic;
SIGNAL \E[6][0]~input_o\ : std_logic;
SIGNAL \E[3][0]~input_o\ : std_logic;
SIGNAL \E[1][0]~input_o\ : std_logic;
SIGNAL \S[1]~input_o\ : std_logic;
SIGNAL \E[0][0]~input_o\ : std_logic;
SIGNAL \S[0]~input_o\ : std_logic;
SIGNAL \Mux0~1_combout\ : std_logic;
SIGNAL \E[2][0]~input_o\ : std_logic;
SIGNAL \Mux0~2_combout\ : std_logic;
SIGNAL \E[4][0]~input_o\ : std_logic;
SIGNAL \S[2]~input_o\ : std_logic;
SIGNAL \Mux0~3_combout\ : std_logic;
SIGNAL \Mux0~0_combout\ : std_logic;
SIGNAL \Mux0~4_combout\ : std_logic;
SIGNAL \E[5][0]~input_o\ : std_logic;
SIGNAL \Mux0~5_combout\ : std_logic;
SIGNAL \Mux16~0_combout\ : std_logic;
SIGNAL \Mux16~0clkctrl_outclk\ : std_logic;
SIGNAL \R[0]$latch~combout\ : std_logic;
SIGNAL \E[6][1]~input_o\ : std_logic;
SIGNAL \E[5][1]~input_o\ : std_logic;
SIGNAL \E[4][1]~input_o\ : std_logic;
SIGNAL \Mux1~2_combout\ : std_logic;
SIGNAL \E[1][1]~input_o\ : std_logic;
SIGNAL \E[2][1]~input_o\ : std_logic;
SIGNAL \E[0][1]~input_o\ : std_logic;
SIGNAL \Mux1~0_combout\ : std_logic;
SIGNAL \E[3][1]~input_o\ : std_logic;
SIGNAL \Mux1~1_combout\ : std_logic;
SIGNAL \Mux1~3_combout\ : std_logic;
SIGNAL \R[1]$latch~combout\ : std_logic;
SIGNAL \E[6][2]~input_o\ : std_logic;
SIGNAL \E[4][2]~input_o\ : std_logic;
SIGNAL \E[3][2]~input_o\ : std_logic;
SIGNAL \E[2][2]~input_o\ : std_logic;
SIGNAL \E[0][2]~input_o\ : std_logic;
SIGNAL \E[1][2]~input_o\ : std_logic;
SIGNAL \Mux2~0_combout\ : std_logic;
SIGNAL \Mux2~1_combout\ : std_logic;
SIGNAL \Mux2~2_combout\ : std_logic;
SIGNAL \E[5][2]~input_o\ : std_logic;
SIGNAL \Mux2~3_combout\ : std_logic;
SIGNAL \R[2]$latch~combout\ : std_logic;
SIGNAL \E[5][3]~input_o\ : std_logic;
SIGNAL \E[4][3]~input_o\ : std_logic;
SIGNAL \Mux3~2_combout\ : std_logic;
SIGNAL \E[6][3]~input_o\ : std_logic;
SIGNAL \E[2][3]~input_o\ : std_logic;
SIGNAL \E[0][3]~input_o\ : std_logic;
SIGNAL \Mux3~0_combout\ : std_logic;
SIGNAL \E[3][3]~input_o\ : std_logic;
SIGNAL \E[1][3]~input_o\ : std_logic;
SIGNAL \Mux3~1_combout\ : std_logic;
SIGNAL \Mux3~3_combout\ : std_logic;
SIGNAL \R[3]$latch~combout\ : std_logic;
SIGNAL \E[2][4]~input_o\ : std_logic;
SIGNAL \E[1][4]~input_o\ : std_logic;
SIGNAL \E[0][4]~input_o\ : std_logic;
SIGNAL \Mux4~0_combout\ : std_logic;
SIGNAL \E[3][4]~input_o\ : std_logic;
SIGNAL \Mux4~1_combout\ : std_logic;
SIGNAL \E[4][4]~input_o\ : std_logic;
SIGNAL \Mux4~2_combout\ : std_logic;
SIGNAL \E[5][4]~input_o\ : std_logic;
SIGNAL \E[6][4]~input_o\ : std_logic;
SIGNAL \Mux4~3_combout\ : std_logic;
SIGNAL \R[4]$latch~combout\ : std_logic;
SIGNAL \E[6][5]~input_o\ : std_logic;
SIGNAL \E[3][5]~input_o\ : std_logic;
SIGNAL \E[2][5]~input_o\ : std_logic;
SIGNAL \E[0][5]~input_o\ : std_logic;
SIGNAL \Mux5~0_combout\ : std_logic;
SIGNAL \E[1][5]~input_o\ : std_logic;
SIGNAL \Mux5~1_combout\ : std_logic;
SIGNAL \E[5][5]~input_o\ : std_logic;
SIGNAL \E[4][5]~input_o\ : std_logic;
SIGNAL \Mux5~2_combout\ : std_logic;
SIGNAL \Mux5~3_combout\ : std_logic;
SIGNAL \R[5]$latch~combout\ : std_logic;
SIGNAL \E[2][6]~input_o\ : std_logic;
SIGNAL \E[1][6]~input_o\ : std_logic;
SIGNAL \E[0][6]~input_o\ : std_logic;
SIGNAL \Mux6~0_combout\ : std_logic;
SIGNAL \E[3][6]~input_o\ : std_logic;
SIGNAL \Mux6~1_combout\ : std_logic;
SIGNAL \E[4][6]~input_o\ : std_logic;
SIGNAL \Mux6~2_combout\ : std_logic;
SIGNAL \E[6][6]~input_o\ : std_logic;
SIGNAL \E[5][6]~input_o\ : std_logic;
SIGNAL \Mux6~3_combout\ : std_logic;
SIGNAL \R[6]$latch~combout\ : std_logic;
SIGNAL \E[5][7]~input_o\ : std_logic;
SIGNAL \E[4][7]~input_o\ : std_logic;
SIGNAL \Mux7~2_combout\ : std_logic;
SIGNAL \E[6][7]~input_o\ : std_logic;
SIGNAL \E[2][7]~input_o\ : std_logic;
SIGNAL \E[0][7]~input_o\ : std_logic;
SIGNAL \Mux7~0_combout\ : std_logic;
SIGNAL \E[1][7]~input_o\ : std_logic;
SIGNAL \E[3][7]~input_o\ : std_logic;
SIGNAL \Mux7~1_combout\ : std_logic;
SIGNAL \Mux7~3_combout\ : std_logic;
SIGNAL \R[7]$latch~combout\ : std_logic;
SIGNAL \E[5][8]~input_o\ : std_logic;
SIGNAL \E[4][8]~input_o\ : std_logic;
SIGNAL \E[3][8]~input_o\ : std_logic;
SIGNAL \E[1][8]~input_o\ : std_logic;
SIGNAL \E[0][8]~input_o\ : std_logic;
SIGNAL \Mux8~0_combout\ : std_logic;
SIGNAL \E[2][8]~input_o\ : std_logic;
SIGNAL \Mux8~1_combout\ : std_logic;
SIGNAL \Mux8~2_combout\ : std_logic;
SIGNAL \E[6][8]~input_o\ : std_logic;
SIGNAL \Mux8~3_combout\ : std_logic;
SIGNAL \R[8]$latch~combout\ : std_logic;
SIGNAL \E[5][9]~input_o\ : std_logic;
SIGNAL \E[4][9]~input_o\ : std_logic;
SIGNAL \Mux9~2_combout\ : std_logic;
SIGNAL \E[1][9]~input_o\ : std_logic;
SIGNAL \E[0][9]~input_o\ : std_logic;
SIGNAL \E[2][9]~input_o\ : std_logic;
SIGNAL \Mux9~0_combout\ : std_logic;
SIGNAL \E[3][9]~input_o\ : std_logic;
SIGNAL \Mux9~1_combout\ : std_logic;
SIGNAL \E[6][9]~input_o\ : std_logic;
SIGNAL \Mux9~3_combout\ : std_logic;
SIGNAL \R[9]$latch~combout\ : std_logic;
SIGNAL \E[2][10]~input_o\ : std_logic;
SIGNAL \E[1][10]~input_o\ : std_logic;
SIGNAL \E[0][10]~input_o\ : std_logic;
SIGNAL \Mux10~0_combout\ : std_logic;
SIGNAL \E[3][10]~input_o\ : std_logic;
SIGNAL \Mux10~1_combout\ : std_logic;
SIGNAL \E[4][10]~input_o\ : std_logic;
SIGNAL \Mux10~2_combout\ : std_logic;
SIGNAL \E[6][10]~input_o\ : std_logic;
SIGNAL \E[5][10]~input_o\ : std_logic;
SIGNAL \Mux10~3_combout\ : std_logic;
SIGNAL \R[10]$latch~combout\ : std_logic;
SIGNAL \E[4][11]~input_o\ : std_logic;
SIGNAL \E[5][11]~input_o\ : std_logic;
SIGNAL \Mux11~2_combout\ : std_logic;
SIGNAL \E[2][11]~input_o\ : std_logic;
SIGNAL \E[0][11]~input_o\ : std_logic;
SIGNAL \Mux11~0_combout\ : std_logic;
SIGNAL \E[1][11]~input_o\ : std_logic;
SIGNAL \E[3][11]~input_o\ : std_logic;
SIGNAL \Mux11~1_combout\ : std_logic;
SIGNAL \E[6][11]~input_o\ : std_logic;
SIGNAL \Mux11~3_combout\ : std_logic;
SIGNAL \R[11]$latch~combout\ : std_logic;
SIGNAL \E[6][12]~input_o\ : std_logic;
SIGNAL \E[4][12]~input_o\ : std_logic;
SIGNAL \E[3][12]~input_o\ : std_logic;
SIGNAL \E[2][12]~input_o\ : std_logic;
SIGNAL \E[1][12]~input_o\ : std_logic;
SIGNAL \E[0][12]~input_o\ : std_logic;
SIGNAL \Mux12~0_combout\ : std_logic;
SIGNAL \Mux12~1_combout\ : std_logic;
SIGNAL \Mux12~2_combout\ : std_logic;
SIGNAL \E[5][12]~input_o\ : std_logic;
SIGNAL \Mux12~3_combout\ : std_logic;
SIGNAL \R[12]$latch~combout\ : std_logic;
SIGNAL \E[2][13]~input_o\ : std_logic;
SIGNAL \E[0][13]~input_o\ : std_logic;
SIGNAL \Mux13~0_combout\ : std_logic;
SIGNAL \E[1][13]~input_o\ : std_logic;
SIGNAL \E[3][13]~input_o\ : std_logic;
SIGNAL \Mux13~1_combout\ : std_logic;
SIGNAL \E[6][13]~input_o\ : std_logic;
SIGNAL \E[5][13]~input_o\ : std_logic;
SIGNAL \E[4][13]~input_o\ : std_logic;
SIGNAL \Mux13~2_combout\ : std_logic;
SIGNAL \Mux13~3_combout\ : std_logic;
SIGNAL \R[13]$latch~combout\ : std_logic;
SIGNAL \E[2][14]~input_o\ : std_logic;
SIGNAL \E[0][14]~input_o\ : std_logic;
SIGNAL \Mux14~0_combout\ : std_logic;
SIGNAL \E[1][14]~input_o\ : std_logic;
SIGNAL \E[3][14]~input_o\ : std_logic;
SIGNAL \Mux14~1_combout\ : std_logic;
SIGNAL \E[4][14]~input_o\ : std_logic;
SIGNAL \Mux14~2_combout\ : std_logic;
SIGNAL \E[5][14]~input_o\ : std_logic;
SIGNAL \E[6][14]~input_o\ : std_logic;
SIGNAL \Mux14~3_combout\ : std_logic;
SIGNAL \R[14]$latch~combout\ : std_logic;
SIGNAL \E[3][15]~input_o\ : std_logic;
SIGNAL \E[0][15]~input_o\ : std_logic;
SIGNAL \E[2][15]~input_o\ : std_logic;
SIGNAL \Mux15~0_combout\ : std_logic;
SIGNAL \E[1][15]~input_o\ : std_logic;
SIGNAL \Mux15~1_combout\ : std_logic;
SIGNAL \E[4][15]~input_o\ : std_logic;
SIGNAL \E[5][15]~input_o\ : std_logic;
SIGNAL \Mux15~2_combout\ : std_logic;
SIGNAL \E[6][15]~input_o\ : std_logic;
SIGNAL \Mux15~3_combout\ : std_logic;
SIGNAL \R[15]$latch~combout\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_E <= E;
ww_S <= S;
R <= ww_R;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\~QUARTUS_CREATED_ADC1~_CHSEL_bus\ <= (\~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\);

\~QUARTUS_CREATED_ADC2~_CHSEL_bus\ <= (\~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\);

\Mux16~0clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \Mux16~0_combout\);
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: LCCOMB_X44_Y45_N20
\~QUARTUS_CREATED_GND~I\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \~QUARTUS_CREATED_GND~I_combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~QUARTUS_CREATED_GND~I_combout\);

-- Location: IOOBUF_X0_Y12_N2
\R[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[0]$latch~combout\,
	devoe => ww_devoe,
	o => \R[0]~output_o\);

-- Location: IOOBUF_X49_Y54_N2
\R[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[1]$latch~combout\,
	devoe => ww_devoe,
	o => \R[1]~output_o\);

-- Location: IOOBUF_X0_Y13_N23
\R[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[2]$latch~combout\,
	devoe => ww_devoe,
	o => \R[2]~output_o\);

-- Location: IOOBUF_X46_Y54_N23
\R[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[3]$latch~combout\,
	devoe => ww_devoe,
	o => \R[3]~output_o\);

-- Location: IOOBUF_X0_Y12_N9
\R[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[4]$latch~combout\,
	devoe => ww_devoe,
	o => \R[4]~output_o\);

-- Location: IOOBUF_X51_Y54_N23
\R[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[5]$latch~combout\,
	devoe => ww_devoe,
	o => \R[5]~output_o\);

-- Location: IOOBUF_X54_Y54_N2
\R[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[6]$latch~combout\,
	devoe => ww_devoe,
	o => \R[6]~output_o\);

-- Location: IOOBUF_X36_Y39_N30
\R[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[7]$latch~combout\,
	devoe => ww_devoe,
	o => \R[7]~output_o\);

-- Location: IOOBUF_X54_Y54_N16
\R[8]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[8]$latch~combout\,
	devoe => ww_devoe,
	o => \R[8]~output_o\);

-- Location: IOOBUF_X0_Y37_N23
\R[9]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[9]$latch~combout\,
	devoe => ww_devoe,
	o => \R[9]~output_o\);

-- Location: IOOBUF_X0_Y37_N16
\R[10]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[10]$latch~combout\,
	devoe => ww_devoe,
	o => \R[10]~output_o\);

-- Location: IOOBUF_X0_Y15_N9
\R[11]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[11]$latch~combout\,
	devoe => ww_devoe,
	o => \R[11]~output_o\);

-- Location: IOOBUF_X51_Y54_N30
\R[12]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[12]$latch~combout\,
	devoe => ww_devoe,
	o => \R[12]~output_o\);

-- Location: IOOBUF_X49_Y54_N23
\R[13]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[13]$latch~combout\,
	devoe => ww_devoe,
	o => \R[13]~output_o\);

-- Location: IOOBUF_X0_Y10_N16
\R[14]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[14]$latch~combout\,
	devoe => ww_devoe,
	o => \R[14]~output_o\);

-- Location: IOOBUF_X0_Y10_N23
\R[15]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[15]$latch~combout\,
	devoe => ww_devoe,
	o => \R[15]~output_o\);

-- Location: IOIBUF_X0_Y30_N1
\E[6][0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(0),
	o => \E[6][0]~input_o\);

-- Location: IOIBUF_X0_Y25_N1
\E[3][0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(0),
	o => \E[3][0]~input_o\);

-- Location: IOIBUF_X0_Y28_N8
\E[1][0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(0),
	o => \E[1][0]~input_o\);

-- Location: IOIBUF_X0_Y35_N8
\S[1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S(1),
	o => \S[1]~input_o\);

-- Location: IOIBUF_X0_Y12_N22
\E[0][0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(0),
	o => \E[0][0]~input_o\);

-- Location: IOIBUF_X0_Y34_N15
\S[0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S(0),
	o => \S[0]~input_o\);

-- Location: LCCOMB_X1_Y30_N8
\Mux0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux0~1_combout\ = (\S[1]~input_o\ & (((\S[0]~input_o\)))) # (!\S[1]~input_o\ & ((\S[0]~input_o\ & (\E[1][0]~input_o\)) # (!\S[0]~input_o\ & ((\E[0][0]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[1][0]~input_o\,
	datab => \S[1]~input_o\,
	datac => \E[0][0]~input_o\,
	datad => \S[0]~input_o\,
	combout => \Mux0~1_combout\);

-- Location: IOIBUF_X0_Y16_N8
\E[2][0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(0),
	o => \E[2][0]~input_o\);

-- Location: LCCOMB_X1_Y30_N30
\Mux0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux0~2_combout\ = (\Mux0~1_combout\ & ((\E[3][0]~input_o\) # ((!\S[1]~input_o\)))) # (!\Mux0~1_combout\ & (((\E[2][0]~input_o\ & \S[1]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[3][0]~input_o\,
	datab => \Mux0~1_combout\,
	datac => \E[2][0]~input_o\,
	datad => \S[1]~input_o\,
	combout => \Mux0~2_combout\);

-- Location: IOIBUF_X0_Y13_N1
\E[4][0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(0),
	o => \E[4][0]~input_o\);

-- Location: IOIBUF_X26_Y39_N1
\S[2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S(2),
	o => \S[2]~input_o\);

-- Location: LCCOMB_X8_Y34_N28
\Mux0~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux0~3_combout\ = (\S[1]~input_o\) # (!\S[2]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \S[1]~input_o\,
	datad => \S[2]~input_o\,
	combout => \Mux0~3_combout\);

-- Location: LCCOMB_X1_Y34_N24
\Mux0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux0~0_combout\ = (\S[2]~input_o\ & ((\S[1]~input_o\) # (\S[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[1]~input_o\,
	datac => \S[0]~input_o\,
	datad => \S[2]~input_o\,
	combout => \Mux0~0_combout\);

-- Location: LCCOMB_X1_Y30_N28
\Mux0~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux0~4_combout\ = (\Mux0~3_combout\ & ((\Mux0~2_combout\) # ((\Mux0~0_combout\)))) # (!\Mux0~3_combout\ & (((\E[4][0]~input_o\ & !\Mux0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux0~2_combout\,
	datab => \E[4][0]~input_o\,
	datac => \Mux0~3_combout\,
	datad => \Mux0~0_combout\,
	combout => \Mux0~4_combout\);

-- Location: IOIBUF_X0_Y29_N8
\E[5][0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(0),
	o => \E[5][0]~input_o\);

-- Location: LCCOMB_X1_Y30_N22
\Mux0~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux0~5_combout\ = (\Mux0~4_combout\ & ((\E[6][0]~input_o\) # ((!\Mux0~0_combout\)))) # (!\Mux0~4_combout\ & (((\Mux0~0_combout\ & \E[5][0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[6][0]~input_o\,
	datab => \Mux0~4_combout\,
	datac => \Mux0~0_combout\,
	datad => \E[5][0]~input_o\,
	combout => \Mux0~5_combout\);

-- Location: LCCOMB_X1_Y38_N14
\Mux16~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux16~0_combout\ = (\S[1]~input_o\ & (\S[2]~input_o\ & \S[0]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[1]~input_o\,
	datab => \S[2]~input_o\,
	datac => \S[0]~input_o\,
	combout => \Mux16~0_combout\);

-- Location: CLKCTRL_G1
\Mux16~0clkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \Mux16~0clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \Mux16~0clkctrl_outclk\);

-- Location: LCCOMB_X1_Y30_N16
\R[0]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[0]$latch~combout\ = (GLOBAL(\Mux16~0clkctrl_outclk\) & (\R[0]$latch~combout\)) # (!GLOBAL(\Mux16~0clkctrl_outclk\) & ((\Mux0~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \R[0]$latch~combout\,
	datac => \Mux0~5_combout\,
	datad => \Mux16~0clkctrl_outclk\,
	combout => \R[0]$latch~combout\);

-- Location: IOIBUF_X56_Y54_N29
\E[6][1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(1),
	o => \E[6][1]~input_o\);

-- Location: IOIBUF_X46_Y54_N8
\E[5][1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(1),
	o => \E[5][1]~input_o\);

-- Location: IOIBUF_X46_Y54_N29
\E[4][1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(1),
	o => \E[4][1]~input_o\);

-- Location: LCCOMB_X14_Y34_N4
\Mux1~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux1~2_combout\ = (\Mux0~3_combout\ & (((\Mux0~0_combout\)))) # (!\Mux0~3_combout\ & ((\Mux0~0_combout\ & (\E[5][1]~input_o\)) # (!\Mux0~0_combout\ & ((\E[4][1]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[5][1]~input_o\,
	datab => \Mux0~3_combout\,
	datac => \Mux0~0_combout\,
	datad => \E[4][1]~input_o\,
	combout => \Mux1~2_combout\);

-- Location: IOIBUF_X49_Y54_N8
\E[1][1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(1),
	o => \E[1][1]~input_o\);

-- Location: IOIBUF_X22_Y39_N15
\E[2][1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(1),
	o => \E[2][1]~input_o\);

-- Location: IOIBUF_X46_Y54_N1
\E[0][1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(1),
	o => \E[0][1]~input_o\);

-- Location: LCCOMB_X14_Y34_N28
\Mux1~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux1~0_combout\ = (\S[0]~input_o\ & (((\S[1]~input_o\)))) # (!\S[0]~input_o\ & ((\S[1]~input_o\ & (\E[2][1]~input_o\)) # (!\S[1]~input_o\ & ((\E[0][1]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \E[2][1]~input_o\,
	datac => \S[1]~input_o\,
	datad => \E[0][1]~input_o\,
	combout => \Mux1~0_combout\);

-- Location: IOIBUF_X20_Y39_N8
\E[3][1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(1),
	o => \E[3][1]~input_o\);

-- Location: LCCOMB_X14_Y34_N18
\Mux1~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux1~1_combout\ = (\S[0]~input_o\ & ((\Mux1~0_combout\ & ((\E[3][1]~input_o\))) # (!\Mux1~0_combout\ & (\E[1][1]~input_o\)))) # (!\S[0]~input_o\ & (((\Mux1~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \E[1][1]~input_o\,
	datac => \Mux1~0_combout\,
	datad => \E[3][1]~input_o\,
	combout => \Mux1~1_combout\);

-- Location: LCCOMB_X14_Y34_N22
\Mux1~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux1~3_combout\ = (\Mux1~2_combout\ & ((\E[6][1]~input_o\) # ((!\Mux0~3_combout\)))) # (!\Mux1~2_combout\ & (((\Mux1~1_combout\ & \Mux0~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[6][1]~input_o\,
	datab => \Mux1~2_combout\,
	datac => \Mux1~1_combout\,
	datad => \Mux0~3_combout\,
	combout => \Mux1~3_combout\);

-- Location: LCCOMB_X14_Y34_N24
\R[1]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[1]$latch~combout\ = (GLOBAL(\Mux16~0clkctrl_outclk\) & ((\R[1]$latch~combout\))) # (!GLOBAL(\Mux16~0clkctrl_outclk\) & (\Mux1~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux1~3_combout\,
	datab => \R[1]$latch~combout\,
	datad => \Mux16~0clkctrl_outclk\,
	combout => \R[1]$latch~combout\);

-- Location: IOIBUF_X78_Y30_N15
\E[6][2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(2),
	o => \E[6][2]~input_o\);

-- Location: IOIBUF_X0_Y27_N1
\E[4][2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(2),
	o => \E[4][2]~input_o\);

-- Location: IOIBUF_X0_Y29_N1
\E[3][2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(2),
	o => \E[3][2]~input_o\);

-- Location: IOIBUF_X0_Y30_N8
\E[2][2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(2),
	o => \E[2][2]~input_o\);

-- Location: IOIBUF_X0_Y25_N8
\E[0][2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(2),
	o => \E[0][2]~input_o\);

-- Location: IOIBUF_X0_Y27_N15
\E[1][2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(2),
	o => \E[1][2]~input_o\);

-- Location: LCCOMB_X1_Y30_N24
\Mux2~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux2~0_combout\ = (\S[1]~input_o\ & (((\S[0]~input_o\)))) # (!\S[1]~input_o\ & ((\S[0]~input_o\ & ((\E[1][2]~input_o\))) # (!\S[0]~input_o\ & (\E[0][2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[0][2]~input_o\,
	datab => \S[1]~input_o\,
	datac => \E[1][2]~input_o\,
	datad => \S[0]~input_o\,
	combout => \Mux2~0_combout\);

-- Location: LCCOMB_X1_Y30_N2
\Mux2~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux2~1_combout\ = (\Mux2~0_combout\ & ((\E[3][2]~input_o\) # ((!\S[1]~input_o\)))) # (!\Mux2~0_combout\ & (((\E[2][2]~input_o\ & \S[1]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[3][2]~input_o\,
	datab => \E[2][2]~input_o\,
	datac => \Mux2~0_combout\,
	datad => \S[1]~input_o\,
	combout => \Mux2~1_combout\);

-- Location: LCCOMB_X1_Y30_N20
\Mux2~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux2~2_combout\ = (\Mux0~3_combout\ & (((\Mux2~1_combout\) # (\Mux0~0_combout\)))) # (!\Mux0~3_combout\ & (\E[4][2]~input_o\ & ((!\Mux0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[4][2]~input_o\,
	datab => \Mux2~1_combout\,
	datac => \Mux0~3_combout\,
	datad => \Mux0~0_combout\,
	combout => \Mux2~2_combout\);

-- Location: IOIBUF_X0_Y30_N15
\E[5][2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(2),
	o => \E[5][2]~input_o\);

-- Location: LCCOMB_X1_Y30_N6
\Mux2~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux2~3_combout\ = (\Mux2~2_combout\ & ((\E[6][2]~input_o\) # ((!\Mux0~0_combout\)))) # (!\Mux2~2_combout\ & (((\Mux0~0_combout\ & \E[5][2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[6][2]~input_o\,
	datab => \Mux2~2_combout\,
	datac => \Mux0~0_combout\,
	datad => \E[5][2]~input_o\,
	combout => \Mux2~3_combout\);

-- Location: LCCOMB_X1_Y30_N26
\R[2]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[2]$latch~combout\ = (GLOBAL(\Mux16~0clkctrl_outclk\) & (\R[2]$latch~combout\)) # (!GLOBAL(\Mux16~0clkctrl_outclk\) & ((\Mux2~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[2]$latch~combout\,
	datac => \Mux2~3_combout\,
	datad => \Mux16~0clkctrl_outclk\,
	combout => \R[2]$latch~combout\);

-- Location: IOIBUF_X22_Y39_N22
\E[5][3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(3),
	o => \E[5][3]~input_o\);

-- Location: IOIBUF_X22_Y39_N29
\E[4][3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(3),
	o => \E[4][3]~input_o\);

-- Location: LCCOMB_X14_Y34_N16
\Mux3~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux3~2_combout\ = (\Mux0~3_combout\ & (((\Mux0~0_combout\)))) # (!\Mux0~3_combout\ & ((\Mux0~0_combout\ & (\E[5][3]~input_o\)) # (!\Mux0~0_combout\ & ((\E[4][3]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[5][3]~input_o\,
	datab => \Mux0~3_combout\,
	datac => \Mux0~0_combout\,
	datad => \E[4][3]~input_o\,
	combout => \Mux3~2_combout\);

-- Location: IOIBUF_X0_Y15_N1
\E[6][3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(3),
	o => \E[6][3]~input_o\);

-- Location: IOIBUF_X26_Y39_N8
\E[2][3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(3),
	o => \E[2][3]~input_o\);

-- Location: IOIBUF_X46_Y54_N15
\E[0][3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(3),
	o => \E[0][3]~input_o\);

-- Location: LCCOMB_X14_Y34_N20
\Mux3~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux3~0_combout\ = (\S[0]~input_o\ & (((\S[1]~input_o\)))) # (!\S[0]~input_o\ & ((\S[1]~input_o\ & (\E[2][3]~input_o\)) # (!\S[1]~input_o\ & ((\E[0][3]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \E[2][3]~input_o\,
	datac => \S[1]~input_o\,
	datad => \E[0][3]~input_o\,
	combout => \Mux3~0_combout\);

-- Location: IOIBUF_X20_Y39_N1
\E[3][3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(3),
	o => \E[3][3]~input_o\);

-- Location: IOIBUF_X20_Y39_N15
\E[1][3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(3),
	o => \E[1][3]~input_o\);

-- Location: LCCOMB_X14_Y34_N30
\Mux3~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux3~1_combout\ = (\Mux3~0_combout\ & (((\E[3][3]~input_o\)) # (!\S[0]~input_o\))) # (!\Mux3~0_combout\ & (\S[0]~input_o\ & ((\E[1][3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux3~0_combout\,
	datab => \S[0]~input_o\,
	datac => \E[3][3]~input_o\,
	datad => \E[1][3]~input_o\,
	combout => \Mux3~1_combout\);

-- Location: LCCOMB_X14_Y34_N6
\Mux3~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux3~3_combout\ = (\Mux3~2_combout\ & ((\E[6][3]~input_o\) # ((!\Mux0~3_combout\)))) # (!\Mux3~2_combout\ & (((\Mux3~1_combout\ & \Mux0~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux3~2_combout\,
	datab => \E[6][3]~input_o\,
	datac => \Mux3~1_combout\,
	datad => \Mux0~3_combout\,
	combout => \Mux3~3_combout\);

-- Location: LCCOMB_X14_Y34_N14
\R[3]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[3]$latch~combout\ = (GLOBAL(\Mux16~0clkctrl_outclk\) & ((\R[3]$latch~combout\))) # (!GLOBAL(\Mux16~0clkctrl_outclk\) & (\Mux3~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux3~3_combout\,
	datac => \R[3]$latch~combout\,
	datad => \Mux16~0clkctrl_outclk\,
	combout => \R[3]$latch~combout\);

-- Location: IOIBUF_X0_Y25_N22
\E[2][4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(4),
	o => \E[2][4]~input_o\);

-- Location: IOIBUF_X78_Y30_N8
\E[1][4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(4),
	o => \E[1][4]~input_o\);

-- Location: IOIBUF_X0_Y30_N22
\E[0][4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(4),
	o => \E[0][4]~input_o\);

-- Location: LCCOMB_X1_Y30_N12
\Mux4~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux4~0_combout\ = (\S[1]~input_o\ & (((\S[0]~input_o\)))) # (!\S[1]~input_o\ & ((\S[0]~input_o\ & (\E[1][4]~input_o\)) # (!\S[0]~input_o\ & ((\E[0][4]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[1][4]~input_o\,
	datab => \S[1]~input_o\,
	datac => \E[0][4]~input_o\,
	datad => \S[0]~input_o\,
	combout => \Mux4~0_combout\);

-- Location: IOIBUF_X0_Y27_N22
\E[3][4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(4),
	o => \E[3][4]~input_o\);

-- Location: LCCOMB_X1_Y30_N14
\Mux4~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux4~1_combout\ = (\Mux4~0_combout\ & (((\E[3][4]~input_o\) # (!\S[1]~input_o\)))) # (!\Mux4~0_combout\ & (\E[2][4]~input_o\ & ((\S[1]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[2][4]~input_o\,
	datab => \Mux4~0_combout\,
	datac => \E[3][4]~input_o\,
	datad => \S[1]~input_o\,
	combout => \Mux4~1_combout\);

-- Location: IOIBUF_X0_Y28_N1
\E[4][4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(4),
	o => \E[4][4]~input_o\);

-- Location: LCCOMB_X1_Y30_N0
\Mux4~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux4~2_combout\ = (\Mux0~3_combout\ & ((\Mux4~1_combout\) # ((\Mux0~0_combout\)))) # (!\Mux0~3_combout\ & (((\E[4][4]~input_o\ & !\Mux0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux4~1_combout\,
	datab => \E[4][4]~input_o\,
	datac => \Mux0~3_combout\,
	datad => \Mux0~0_combout\,
	combout => \Mux4~2_combout\);

-- Location: IOIBUF_X0_Y12_N15
\E[5][4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(4),
	o => \E[5][4]~input_o\);

-- Location: IOIBUF_X0_Y27_N8
\E[6][4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(4),
	o => \E[6][4]~input_o\);

-- Location: LCCOMB_X1_Y30_N10
\Mux4~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux4~3_combout\ = (\Mux4~2_combout\ & (((\E[6][4]~input_o\) # (!\Mux0~0_combout\)))) # (!\Mux4~2_combout\ & (\E[5][4]~input_o\ & (\Mux0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux4~2_combout\,
	datab => \E[5][4]~input_o\,
	datac => \Mux0~0_combout\,
	datad => \E[6][4]~input_o\,
	combout => \Mux4~3_combout\);

-- Location: LCCOMB_X1_Y30_N4
\R[4]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[4]$latch~combout\ = (GLOBAL(\Mux16~0clkctrl_outclk\) & ((\R[4]$latch~combout\))) # (!GLOBAL(\Mux16~0clkctrl_outclk\) & (\Mux4~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mux4~3_combout\,
	datac => \R[4]$latch~combout\,
	datad => \Mux16~0clkctrl_outclk\,
	combout => \R[4]$latch~combout\);

-- Location: IOIBUF_X49_Y54_N15
\E[6][5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(5),
	o => \E[6][5]~input_o\);

-- Location: IOIBUF_X49_Y54_N29
\E[3][5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(5),
	o => \E[3][5]~input_o\);

-- Location: IOIBUF_X31_Y39_N1
\E[2][5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(5),
	o => \E[2][5]~input_o\);

-- Location: IOIBUF_X29_Y39_N15
\E[0][5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(5),
	o => \E[0][5]~input_o\);

-- Location: LCCOMB_X23_Y34_N28
\Mux5~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux5~0_combout\ = (\S[0]~input_o\ & (((\S[1]~input_o\)))) # (!\S[0]~input_o\ & ((\S[1]~input_o\ & (\E[2][5]~input_o\)) # (!\S[1]~input_o\ & ((\E[0][5]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \E[2][5]~input_o\,
	datac => \E[0][5]~input_o\,
	datad => \S[1]~input_o\,
	combout => \Mux5~0_combout\);

-- Location: IOIBUF_X26_Y39_N22
\E[1][5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(5),
	o => \E[1][5]~input_o\);

-- Location: LCCOMB_X23_Y34_N30
\Mux5~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux5~1_combout\ = (\Mux5~0_combout\ & ((\E[3][5]~input_o\) # ((!\S[0]~input_o\)))) # (!\Mux5~0_combout\ & (((\E[1][5]~input_o\ & \S[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[3][5]~input_o\,
	datab => \Mux5~0_combout\,
	datac => \E[1][5]~input_o\,
	datad => \S[0]~input_o\,
	combout => \Mux5~1_combout\);

-- Location: IOIBUF_X24_Y0_N29
\E[5][5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(5),
	o => \E[5][5]~input_o\);

-- Location: IOIBUF_X24_Y39_N1
\E[4][5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(5),
	o => \E[4][5]~input_o\);

-- Location: LCCOMB_X23_Y34_N16
\Mux5~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux5~2_combout\ = (\Mux0~3_combout\ & (((\Mux0~0_combout\)))) # (!\Mux0~3_combout\ & ((\Mux0~0_combout\ & (\E[5][5]~input_o\)) # (!\Mux0~0_combout\ & ((\E[4][5]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[5][5]~input_o\,
	datab => \Mux0~3_combout\,
	datac => \Mux0~0_combout\,
	datad => \E[4][5]~input_o\,
	combout => \Mux5~2_combout\);

-- Location: LCCOMB_X23_Y34_N18
\Mux5~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux5~3_combout\ = (\Mux5~2_combout\ & ((\E[6][5]~input_o\) # ((!\Mux0~3_combout\)))) # (!\Mux5~2_combout\ & (((\Mux5~1_combout\ & \Mux0~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[6][5]~input_o\,
	datab => \Mux5~1_combout\,
	datac => \Mux5~2_combout\,
	datad => \Mux0~3_combout\,
	combout => \Mux5~3_combout\);

-- Location: LCCOMB_X23_Y34_N8
\R[5]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[5]$latch~combout\ = (GLOBAL(\Mux16~0clkctrl_outclk\) & ((\R[5]$latch~combout\))) # (!GLOBAL(\Mux16~0clkctrl_outclk\) & (\Mux5~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux5~3_combout\,
	datac => \R[5]$latch~combout\,
	datad => \Mux16~0clkctrl_outclk\,
	combout => \R[5]$latch~combout\);

-- Location: IOIBUF_X0_Y10_N8
\E[2][6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(6),
	o => \E[2][6]~input_o\);

-- Location: IOIBUF_X78_Y34_N1
\E[1][6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(6),
	o => \E[1][6]~input_o\);

-- Location: IOIBUF_X36_Y39_N15
\E[0][6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(6),
	o => \E[0][6]~input_o\);

-- Location: LCCOMB_X8_Y34_N10
\Mux6~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux6~0_combout\ = (\S[1]~input_o\ & (((\S[0]~input_o\)))) # (!\S[1]~input_o\ & ((\S[0]~input_o\ & (\E[1][6]~input_o\)) # (!\S[0]~input_o\ & ((\E[0][6]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[1][6]~input_o\,
	datab => \S[1]~input_o\,
	datac => \E[0][6]~input_o\,
	datad => \S[0]~input_o\,
	combout => \Mux6~0_combout\);

-- Location: IOIBUF_X16_Y0_N15
\E[3][6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(6),
	o => \E[3][6]~input_o\);

-- Location: LCCOMB_X8_Y34_N8
\Mux6~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux6~1_combout\ = (\S[1]~input_o\ & ((\Mux6~0_combout\ & ((\E[3][6]~input_o\))) # (!\Mux6~0_combout\ & (\E[2][6]~input_o\)))) # (!\S[1]~input_o\ & (((\Mux6~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[2][6]~input_o\,
	datab => \S[1]~input_o\,
	datac => \Mux6~0_combout\,
	datad => \E[3][6]~input_o\,
	combout => \Mux6~1_combout\);

-- Location: IOIBUF_X14_Y0_N15
\E[4][6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(6),
	o => \E[4][6]~input_o\);

-- Location: LCCOMB_X8_Y34_N14
\Mux6~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux6~2_combout\ = (\Mux0~0_combout\ & (\Mux0~3_combout\)) # (!\Mux0~0_combout\ & ((\Mux0~3_combout\ & (\Mux6~1_combout\)) # (!\Mux0~3_combout\ & ((\E[4][6]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux0~0_combout\,
	datab => \Mux0~3_combout\,
	datac => \Mux6~1_combout\,
	datad => \E[4][6]~input_o\,
	combout => \Mux6~2_combout\);

-- Location: IOIBUF_X78_Y34_N23
\E[6][6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(6),
	o => \E[6][6]~input_o\);

-- Location: IOIBUF_X34_Y39_N15
\E[5][6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(6),
	o => \E[5][6]~input_o\);

-- Location: LCCOMB_X8_Y34_N24
\Mux6~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux6~3_combout\ = (\Mux6~2_combout\ & ((\E[6][6]~input_o\) # ((!\Mux0~0_combout\)))) # (!\Mux6~2_combout\ & (((\E[5][6]~input_o\ & \Mux0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux6~2_combout\,
	datab => \E[6][6]~input_o\,
	datac => \E[5][6]~input_o\,
	datad => \Mux0~0_combout\,
	combout => \Mux6~3_combout\);

-- Location: LCCOMB_X8_Y34_N30
\R[6]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[6]$latch~combout\ = (GLOBAL(\Mux16~0clkctrl_outclk\) & ((\R[6]$latch~combout\))) # (!GLOBAL(\Mux16~0clkctrl_outclk\) & (\Mux6~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux6~3_combout\,
	datac => \R[6]$latch~combout\,
	datad => \Mux16~0clkctrl_outclk\,
	combout => \R[6]$latch~combout\);

-- Location: IOIBUF_X56_Y54_N1
\E[5][7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(7),
	o => \E[5][7]~input_o\);

-- Location: IOIBUF_X54_Y54_N29
\E[4][7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(7),
	o => \E[4][7]~input_o\);

-- Location: LCCOMB_X8_Y34_N26
\Mux7~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux7~2_combout\ = (\Mux0~3_combout\ & (((\Mux0~0_combout\)))) # (!\Mux0~3_combout\ & ((\Mux0~0_combout\ & (\E[5][7]~input_o\)) # (!\Mux0~0_combout\ & ((\E[4][7]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[5][7]~input_o\,
	datab => \Mux0~3_combout\,
	datac => \E[4][7]~input_o\,
	datad => \Mux0~0_combout\,
	combout => \Mux7~2_combout\);

-- Location: IOIBUF_X0_Y13_N8
\E[6][7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(7),
	o => \E[6][7]~input_o\);

-- Location: IOIBUF_X54_Y54_N8
\E[2][7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(7),
	o => \E[2][7]~input_o\);

-- Location: IOIBUF_X14_Y0_N8
\E[0][7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(7),
	o => \E[0][7]~input_o\);

-- Location: LCCOMB_X8_Y34_N18
\Mux7~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux7~0_combout\ = (\S[1]~input_o\ & ((\E[2][7]~input_o\) # ((\S[0]~input_o\)))) # (!\S[1]~input_o\ & (((\E[0][7]~input_o\ & !\S[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[2][7]~input_o\,
	datab => \S[1]~input_o\,
	datac => \E[0][7]~input_o\,
	datad => \S[0]~input_o\,
	combout => \Mux7~0_combout\);

-- Location: IOIBUF_X34_Y39_N8
\E[1][7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(7),
	o => \E[1][7]~input_o\);

-- Location: IOIBUF_X36_Y39_N22
\E[3][7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(7),
	o => \E[3][7]~input_o\);

-- Location: LCCOMB_X8_Y34_N20
\Mux7~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux7~1_combout\ = (\Mux7~0_combout\ & (((\E[3][7]~input_o\) # (!\S[0]~input_o\)))) # (!\Mux7~0_combout\ & (\E[1][7]~input_o\ & (\S[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux7~0_combout\,
	datab => \E[1][7]~input_o\,
	datac => \S[0]~input_o\,
	datad => \E[3][7]~input_o\,
	combout => \Mux7~1_combout\);

-- Location: LCCOMB_X8_Y34_N16
\Mux7~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux7~3_combout\ = (\Mux7~2_combout\ & (((\E[6][7]~input_o\)) # (!\Mux0~3_combout\))) # (!\Mux7~2_combout\ & (\Mux0~3_combout\ & ((\Mux7~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux7~2_combout\,
	datab => \Mux0~3_combout\,
	datac => \E[6][7]~input_o\,
	datad => \Mux7~1_combout\,
	combout => \Mux7~3_combout\);

-- Location: LCCOMB_X8_Y34_N12
\R[7]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[7]$latch~combout\ = (GLOBAL(\Mux16~0clkctrl_outclk\) & ((\R[7]$latch~combout\))) # (!GLOBAL(\Mux16~0clkctrl_outclk\) & (\Mux7~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mux7~3_combout\,
	datac => \Mux16~0clkctrl_outclk\,
	datad => \R[7]$latch~combout\,
	combout => \R[7]$latch~combout\);

-- Location: IOIBUF_X34_Y39_N22
\E[5][8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(8),
	o => \E[5][8]~input_o\);

-- Location: IOIBUF_X34_Y39_N29
\E[4][8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(8),
	o => \E[4][8]~input_o\);

-- Location: IOIBUF_X78_Y34_N8
\E[3][8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(8),
	o => \E[3][8]~input_o\);

-- Location: IOIBUF_X34_Y39_N1
\E[1][8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(8),
	o => \E[1][8]~input_o\);

-- Location: IOIBUF_X14_Y0_N1
\E[0][8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(8),
	o => \E[0][8]~input_o\);

-- Location: LCCOMB_X9_Y34_N8
\Mux8~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux8~0_combout\ = (\S[1]~input_o\ & (((\S[0]~input_o\)))) # (!\S[1]~input_o\ & ((\S[0]~input_o\ & (\E[1][8]~input_o\)) # (!\S[0]~input_o\ & ((\E[0][8]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[1]~input_o\,
	datab => \E[1][8]~input_o\,
	datac => \E[0][8]~input_o\,
	datad => \S[0]~input_o\,
	combout => \Mux8~0_combout\);

-- Location: IOIBUF_X0_Y15_N22
\E[2][8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(8),
	o => \E[2][8]~input_o\);

-- Location: LCCOMB_X9_Y34_N18
\Mux8~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux8~1_combout\ = (\S[1]~input_o\ & ((\Mux8~0_combout\ & (\E[3][8]~input_o\)) # (!\Mux8~0_combout\ & ((\E[2][8]~input_o\))))) # (!\S[1]~input_o\ & (((\Mux8~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[1]~input_o\,
	datab => \E[3][8]~input_o\,
	datac => \Mux8~0_combout\,
	datad => \E[2][8]~input_o\,
	combout => \Mux8~1_combout\);

-- Location: LCCOMB_X9_Y34_N12
\Mux8~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux8~2_combout\ = (\Mux0~0_combout\ & (((\Mux0~3_combout\)))) # (!\Mux0~0_combout\ & ((\Mux0~3_combout\ & ((\Mux8~1_combout\))) # (!\Mux0~3_combout\ & (\E[4][8]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux0~0_combout\,
	datab => \E[4][8]~input_o\,
	datac => \Mux8~1_combout\,
	datad => \Mux0~3_combout\,
	combout => \Mux8~2_combout\);

-- Location: IOIBUF_X78_Y33_N8
\E[6][8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(8),
	o => \E[6][8]~input_o\);

-- Location: LCCOMB_X9_Y34_N6
\Mux8~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux8~3_combout\ = (\Mux8~2_combout\ & (((\E[6][8]~input_o\) # (!\Mux0~0_combout\)))) # (!\Mux8~2_combout\ & (\E[5][8]~input_o\ & ((\Mux0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[5][8]~input_o\,
	datab => \Mux8~2_combout\,
	datac => \E[6][8]~input_o\,
	datad => \Mux0~0_combout\,
	combout => \Mux8~3_combout\);

-- Location: LCCOMB_X9_Y34_N24
\R[8]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[8]$latch~combout\ = (GLOBAL(\Mux16~0clkctrl_outclk\) & (\R[8]$latch~combout\)) # (!GLOBAL(\Mux16~0clkctrl_outclk\) & ((\Mux8~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \R[8]$latch~combout\,
	datac => \Mux8~3_combout\,
	datad => \Mux16~0clkctrl_outclk\,
	combout => \R[8]$latch~combout\);

-- Location: IOIBUF_X0_Y35_N22
\E[5][9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(9),
	o => \E[5][9]~input_o\);

-- Location: IOIBUF_X0_Y13_N15
\E[4][9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(9),
	o => \E[4][9]~input_o\);

-- Location: LCCOMB_X1_Y34_N6
\Mux9~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux9~2_combout\ = (\Mux0~3_combout\ & (((\Mux0~0_combout\)))) # (!\Mux0~3_combout\ & ((\Mux0~0_combout\ & (\E[5][9]~input_o\)) # (!\Mux0~0_combout\ & ((\E[4][9]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux0~3_combout\,
	datab => \E[5][9]~input_o\,
	datac => \E[4][9]~input_o\,
	datad => \Mux0~0_combout\,
	combout => \Mux9~2_combout\);

-- Location: IOIBUF_X78_Y34_N15
\E[1][9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(9),
	o => \E[1][9]~input_o\);

-- Location: IOIBUF_X0_Y3_N1
\E[0][9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(9),
	o => \E[0][9]~input_o\);

-- Location: IOIBUF_X0_Y35_N1
\E[2][9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(9),
	o => \E[2][9]~input_o\);

-- Location: LCCOMB_X1_Y34_N30
\Mux9~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux9~0_combout\ = (\S[0]~input_o\ & (((\S[1]~input_o\)))) # (!\S[0]~input_o\ & ((\S[1]~input_o\ & ((\E[2][9]~input_o\))) # (!\S[1]~input_o\ & (\E[0][9]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[0][9]~input_o\,
	datab => \S[0]~input_o\,
	datac => \S[1]~input_o\,
	datad => \E[2][9]~input_o\,
	combout => \Mux9~0_combout\);

-- Location: IOIBUF_X0_Y36_N1
\E[3][9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(9),
	o => \E[3][9]~input_o\);

-- Location: LCCOMB_X1_Y34_N20
\Mux9~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux9~1_combout\ = (\S[0]~input_o\ & ((\Mux9~0_combout\ & ((\E[3][9]~input_o\))) # (!\Mux9~0_combout\ & (\E[1][9]~input_o\)))) # (!\S[0]~input_o\ & (((\Mux9~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100000111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[1][9]~input_o\,
	datab => \S[0]~input_o\,
	datac => \Mux9~0_combout\,
	datad => \E[3][9]~input_o\,
	combout => \Mux9~1_combout\);

-- Location: IOIBUF_X0_Y34_N1
\E[6][9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(9),
	o => \E[6][9]~input_o\);

-- Location: LCCOMB_X1_Y34_N12
\Mux9~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux9~3_combout\ = (\Mux9~2_combout\ & (((\E[6][9]~input_o\) # (!\Mux0~3_combout\)))) # (!\Mux9~2_combout\ & (\Mux9~1_combout\ & (\Mux0~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux9~2_combout\,
	datab => \Mux9~1_combout\,
	datac => \Mux0~3_combout\,
	datad => \E[6][9]~input_o\,
	combout => \Mux9~3_combout\);

-- Location: LCCOMB_X1_Y34_N10
\R[9]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[9]$latch~combout\ = (GLOBAL(\Mux16~0clkctrl_outclk\) & ((\R[9]$latch~combout\))) # (!GLOBAL(\Mux16~0clkctrl_outclk\) & (\Mux9~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mux16~0clkctrl_outclk\,
	datac => \Mux9~3_combout\,
	datad => \R[9]$latch~combout\,
	combout => \R[9]$latch~combout\);

-- Location: IOIBUF_X0_Y16_N22
\E[2][10]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(10),
	o => \E[2][10]~input_o\);

-- Location: IOIBUF_X0_Y37_N1
\E[1][10]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(10),
	o => \E[1][10]~input_o\);

-- Location: IOIBUF_X0_Y36_N15
\E[0][10]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(10),
	o => \E[0][10]~input_o\);

-- Location: LCCOMB_X1_Y34_N26
\Mux10~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux10~0_combout\ = (\S[0]~input_o\ & ((\E[1][10]~input_o\) # ((\S[1]~input_o\)))) # (!\S[0]~input_o\ & (((!\S[1]~input_o\ & \E[0][10]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[1][10]~input_o\,
	datab => \S[0]~input_o\,
	datac => \S[1]~input_o\,
	datad => \E[0][10]~input_o\,
	combout => \Mux10~0_combout\);

-- Location: IOIBUF_X0_Y34_N8
\E[3][10]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(10),
	o => \E[3][10]~input_o\);

-- Location: LCCOMB_X1_Y34_N28
\Mux10~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux10~1_combout\ = (\Mux10~0_combout\ & (((\E[3][10]~input_o\) # (!\S[1]~input_o\)))) # (!\Mux10~0_combout\ & (\E[2][10]~input_o\ & (\S[1]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[2][10]~input_o\,
	datab => \Mux10~0_combout\,
	datac => \S[1]~input_o\,
	datad => \E[3][10]~input_o\,
	combout => \Mux10~1_combout\);

-- Location: IOIBUF_X0_Y35_N15
\E[4][10]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(10),
	o => \E[4][10]~input_o\);

-- Location: LCCOMB_X1_Y34_N22
\Mux10~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux10~2_combout\ = (\Mux0~3_combout\ & ((\Mux10~1_combout\) # ((\Mux0~0_combout\)))) # (!\Mux0~3_combout\ & (((\E[4][10]~input_o\ & !\Mux0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux0~3_combout\,
	datab => \Mux10~1_combout\,
	datac => \E[4][10]~input_o\,
	datad => \Mux0~0_combout\,
	combout => \Mux10~2_combout\);

-- Location: IOIBUF_X29_Y39_N1
\E[6][10]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(10),
	o => \E[6][10]~input_o\);

-- Location: IOIBUF_X0_Y18_N8
\E[5][10]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(10),
	o => \E[5][10]~input_o\);

-- Location: LCCOMB_X1_Y34_N0
\Mux10~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux10~3_combout\ = (\Mux10~2_combout\ & ((\E[6][10]~input_o\) # ((!\Mux0~0_combout\)))) # (!\Mux10~2_combout\ & (((\E[5][10]~input_o\ & \Mux0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux10~2_combout\,
	datab => \E[6][10]~input_o\,
	datac => \E[5][10]~input_o\,
	datad => \Mux0~0_combout\,
	combout => \Mux10~3_combout\);

-- Location: LCCOMB_X1_Y34_N4
\R[10]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[10]$latch~combout\ = (GLOBAL(\Mux16~0clkctrl_outclk\) & ((\R[10]$latch~combout\))) # (!GLOBAL(\Mux16~0clkctrl_outclk\) & (\Mux10~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mux10~3_combout\,
	datac => \R[10]$latch~combout\,
	datad => \Mux16~0clkctrl_outclk\,
	combout => \R[10]$latch~combout\);

-- Location: IOIBUF_X26_Y39_N15
\E[4][11]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(11),
	o => \E[4][11]~input_o\);

-- Location: IOIBUF_X0_Y34_N22
\E[5][11]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(11),
	o => \E[5][11]~input_o\);

-- Location: LCCOMB_X1_Y34_N18
\Mux11~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux11~2_combout\ = (\Mux0~3_combout\ & (\Mux0~0_combout\)) # (!\Mux0~3_combout\ & ((\Mux0~0_combout\ & ((\E[5][11]~input_o\))) # (!\Mux0~0_combout\ & (\E[4][11]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux0~3_combout\,
	datab => \Mux0~0_combout\,
	datac => \E[4][11]~input_o\,
	datad => \E[5][11]~input_o\,
	combout => \Mux11~2_combout\);

-- Location: IOIBUF_X0_Y36_N8
\E[2][11]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(11),
	o => \E[2][11]~input_o\);

-- Location: IOIBUF_X0_Y16_N1
\E[0][11]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(11),
	o => \E[0][11]~input_o\);

-- Location: LCCOMB_X1_Y34_N14
\Mux11~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux11~0_combout\ = (\S[0]~input_o\ & (((\S[1]~input_o\)))) # (!\S[0]~input_o\ & ((\S[1]~input_o\ & (\E[2][11]~input_o\)) # (!\S[1]~input_o\ & ((\E[0][11]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[2][11]~input_o\,
	datab => \S[0]~input_o\,
	datac => \S[1]~input_o\,
	datad => \E[0][11]~input_o\,
	combout => \Mux11~0_combout\);

-- Location: IOIBUF_X0_Y36_N22
\E[1][11]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(11),
	o => \E[1][11]~input_o\);

-- Location: IOIBUF_X0_Y37_N8
\E[3][11]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(11),
	o => \E[3][11]~input_o\);

-- Location: LCCOMB_X1_Y34_N16
\Mux11~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux11~1_combout\ = (\Mux11~0_combout\ & (((\E[3][11]~input_o\)) # (!\S[0]~input_o\))) # (!\Mux11~0_combout\ & (\S[0]~input_o\ & (\E[1][11]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux11~0_combout\,
	datab => \S[0]~input_o\,
	datac => \E[1][11]~input_o\,
	datad => \E[3][11]~input_o\,
	combout => \Mux11~1_combout\);

-- Location: IOIBUF_X0_Y16_N15
\E[6][11]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(11),
	o => \E[6][11]~input_o\);

-- Location: LCCOMB_X1_Y34_N8
\Mux11~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux11~3_combout\ = (\Mux0~3_combout\ & ((\Mux11~2_combout\ & ((\E[6][11]~input_o\))) # (!\Mux11~2_combout\ & (\Mux11~1_combout\)))) # (!\Mux0~3_combout\ & (\Mux11~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux0~3_combout\,
	datab => \Mux11~2_combout\,
	datac => \Mux11~1_combout\,
	datad => \E[6][11]~input_o\,
	combout => \Mux11~3_combout\);

-- Location: LCCOMB_X1_Y34_N2
\R[11]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[11]$latch~combout\ = (GLOBAL(\Mux16~0clkctrl_outclk\) & (\R[11]$latch~combout\)) # (!GLOBAL(\Mux16~0clkctrl_outclk\) & ((\Mux11~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \R[11]$latch~combout\,
	datac => \Mux11~3_combout\,
	datad => \Mux16~0clkctrl_outclk\,
	combout => \R[11]$latch~combout\);

-- Location: IOIBUF_X31_Y39_N8
\E[6][12]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(12),
	o => \E[6][12]~input_o\);

-- Location: IOIBUF_X51_Y54_N1
\E[4][12]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(12),
	o => \E[4][12]~input_o\);

-- Location: IOIBUF_X29_Y39_N8
\E[3][12]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(12),
	o => \E[3][12]~input_o\);

-- Location: IOIBUF_X24_Y39_N29
\E[2][12]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(12),
	o => \E[2][12]~input_o\);

-- Location: IOIBUF_X54_Y54_N22
\E[1][12]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(12),
	o => \E[1][12]~input_o\);

-- Location: IOIBUF_X31_Y39_N22
\E[0][12]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(12),
	o => \E[0][12]~input_o\);

-- Location: LCCOMB_X23_Y34_N12
\Mux12~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux12~0_combout\ = (\S[0]~input_o\ & ((\E[1][12]~input_o\) # ((\S[1]~input_o\)))) # (!\S[0]~input_o\ & (((\E[0][12]~input_o\ & !\S[1]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \E[1][12]~input_o\,
	datac => \E[0][12]~input_o\,
	datad => \S[1]~input_o\,
	combout => \Mux12~0_combout\);

-- Location: LCCOMB_X23_Y34_N10
\Mux12~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux12~1_combout\ = (\Mux12~0_combout\ & ((\E[3][12]~input_o\) # ((!\S[1]~input_o\)))) # (!\Mux12~0_combout\ & (((\E[2][12]~input_o\ & \S[1]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[3][12]~input_o\,
	datab => \E[2][12]~input_o\,
	datac => \Mux12~0_combout\,
	datad => \S[1]~input_o\,
	combout => \Mux12~1_combout\);

-- Location: LCCOMB_X23_Y34_N4
\Mux12~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux12~2_combout\ = (\Mux0~0_combout\ & (((\Mux0~3_combout\)))) # (!\Mux0~0_combout\ & ((\Mux0~3_combout\ & ((\Mux12~1_combout\))) # (!\Mux0~3_combout\ & (\E[4][12]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[4][12]~input_o\,
	datab => \Mux0~0_combout\,
	datac => \Mux12~1_combout\,
	datad => \Mux0~3_combout\,
	combout => \Mux12~2_combout\);

-- Location: IOIBUF_X24_Y39_N22
\E[5][12]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(12),
	o => \E[5][12]~input_o\);

-- Location: LCCOMB_X23_Y34_N22
\Mux12~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux12~3_combout\ = (\Mux0~0_combout\ & ((\Mux12~2_combout\ & (\E[6][12]~input_o\)) # (!\Mux12~2_combout\ & ((\E[5][12]~input_o\))))) # (!\Mux0~0_combout\ & (((\Mux12~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[6][12]~input_o\,
	datab => \Mux0~0_combout\,
	datac => \Mux12~2_combout\,
	datad => \E[5][12]~input_o\,
	combout => \Mux12~3_combout\);

-- Location: LCCOMB_X23_Y34_N6
\R[12]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[12]$latch~combout\ = (GLOBAL(\Mux16~0clkctrl_outclk\) & (\R[12]$latch~combout\)) # (!GLOBAL(\Mux16~0clkctrl_outclk\) & ((\Mux12~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[12]$latch~combout\,
	datab => \Mux12~3_combout\,
	datad => \Mux16~0clkctrl_outclk\,
	combout => \R[12]$latch~combout\);

-- Location: IOIBUF_X31_Y39_N15
\E[2][13]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(13),
	o => \E[2][13]~input_o\);

-- Location: IOIBUF_X31_Y39_N29
\E[0][13]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(13),
	o => \E[0][13]~input_o\);

-- Location: LCCOMB_X23_Y34_N0
\Mux13~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux13~0_combout\ = (\S[0]~input_o\ & (((\S[1]~input_o\)))) # (!\S[0]~input_o\ & ((\S[1]~input_o\ & (\E[2][13]~input_o\)) # (!\S[1]~input_o\ & ((\E[0][13]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \E[2][13]~input_o\,
	datac => \E[0][13]~input_o\,
	datad => \S[1]~input_o\,
	combout => \Mux13~0_combout\);

-- Location: IOIBUF_X24_Y39_N8
\E[1][13]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(13),
	o => \E[1][13]~input_o\);

-- Location: IOIBUF_X51_Y54_N15
\E[3][13]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(13),
	o => \E[3][13]~input_o\);

-- Location: LCCOMB_X23_Y34_N14
\Mux13~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux13~1_combout\ = (\S[0]~input_o\ & ((\Mux13~0_combout\ & ((\E[3][13]~input_o\))) # (!\Mux13~0_combout\ & (\E[1][13]~input_o\)))) # (!\S[0]~input_o\ & (\Mux13~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \Mux13~0_combout\,
	datac => \E[1][13]~input_o\,
	datad => \E[3][13]~input_o\,
	combout => \Mux13~1_combout\);

-- Location: IOIBUF_X51_Y54_N8
\E[6][13]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(13),
	o => \E[6][13]~input_o\);

-- Location: IOIBUF_X24_Y39_N15
\E[5][13]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(13),
	o => \E[5][13]~input_o\);

-- Location: IOIBUF_X26_Y39_N29
\E[4][13]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(13),
	o => \E[4][13]~input_o\);

-- Location: LCCOMB_X23_Y34_N20
\Mux13~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux13~2_combout\ = (\Mux0~0_combout\ & ((\E[5][13]~input_o\) # ((\Mux0~3_combout\)))) # (!\Mux0~0_combout\ & (((\E[4][13]~input_o\ & !\Mux0~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[5][13]~input_o\,
	datab => \E[4][13]~input_o\,
	datac => \Mux0~0_combout\,
	datad => \Mux0~3_combout\,
	combout => \Mux13~2_combout\);

-- Location: LCCOMB_X23_Y34_N26
\Mux13~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux13~3_combout\ = (\Mux13~2_combout\ & (((\E[6][13]~input_o\) # (!\Mux0~3_combout\)))) # (!\Mux13~2_combout\ & (\Mux13~1_combout\ & ((\Mux0~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux13~1_combout\,
	datab => \E[6][13]~input_o\,
	datac => \Mux13~2_combout\,
	datad => \Mux0~3_combout\,
	combout => \Mux13~3_combout\);

-- Location: LCCOMB_X23_Y34_N24
\R[13]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[13]$latch~combout\ = (GLOBAL(\Mux16~0clkctrl_outclk\) & ((\R[13]$latch~combout\))) # (!GLOBAL(\Mux16~0clkctrl_outclk\) & (\Mux13~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux13~3_combout\,
	datab => \R[13]$latch~combout\,
	datad => \Mux16~0clkctrl_outclk\,
	combout => \R[13]$latch~combout\);

-- Location: IOIBUF_X0_Y15_N15
\E[2][14]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(14),
	o => \E[2][14]~input_o\);

-- Location: IOIBUF_X0_Y26_N1
\E[0][14]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(14),
	o => \E[0][14]~input_o\);

-- Location: LCCOMB_X1_Y26_N4
\Mux14~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux14~0_combout\ = (\S[1]~input_o\ & ((\E[2][14]~input_o\) # ((\S[0]~input_o\)))) # (!\S[1]~input_o\ & (((\E[0][14]~input_o\ & !\S[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[2][14]~input_o\,
	datab => \S[1]~input_o\,
	datac => \E[0][14]~input_o\,
	datad => \S[0]~input_o\,
	combout => \Mux14~0_combout\);

-- Location: IOIBUF_X0_Y23_N8
\E[1][14]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(14),
	o => \E[1][14]~input_o\);

-- Location: IOIBUF_X0_Y23_N22
\E[3][14]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(14),
	o => \E[3][14]~input_o\);

-- Location: LCCOMB_X1_Y26_N26
\Mux14~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux14~1_combout\ = (\Mux14~0_combout\ & (((\E[3][14]~input_o\) # (!\S[0]~input_o\)))) # (!\Mux14~0_combout\ & (\E[1][14]~input_o\ & (\S[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux14~0_combout\,
	datab => \E[1][14]~input_o\,
	datac => \S[0]~input_o\,
	datad => \E[3][14]~input_o\,
	combout => \Mux14~1_combout\);

-- Location: IOIBUF_X0_Y18_N15
\E[4][14]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(14),
	o => \E[4][14]~input_o\);

-- Location: LCCOMB_X1_Y26_N16
\Mux14~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux14~2_combout\ = (\Mux0~3_combout\ & ((\Mux14~1_combout\) # ((\Mux0~0_combout\)))) # (!\Mux0~3_combout\ & (((\E[4][14]~input_o\ & !\Mux0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux14~1_combout\,
	datab => \Mux0~3_combout\,
	datac => \E[4][14]~input_o\,
	datad => \Mux0~0_combout\,
	combout => \Mux14~2_combout\);

-- Location: IOIBUF_X0_Y18_N22
\E[5][14]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(14),
	o => \E[5][14]~input_o\);

-- Location: IOIBUF_X0_Y25_N15
\E[6][14]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(14),
	o => \E[6][14]~input_o\);

-- Location: LCCOMB_X1_Y26_N10
\Mux14~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux14~3_combout\ = (\Mux14~2_combout\ & (((\E[6][14]~input_o\) # (!\Mux0~0_combout\)))) # (!\Mux14~2_combout\ & (\E[5][14]~input_o\ & ((\Mux0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux14~2_combout\,
	datab => \E[5][14]~input_o\,
	datac => \E[6][14]~input_o\,
	datad => \Mux0~0_combout\,
	combout => \Mux14~3_combout\);

-- Location: LCCOMB_X1_Y26_N24
\R[14]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[14]$latch~combout\ = (GLOBAL(\Mux16~0clkctrl_outclk\) & (\R[14]$latch~combout\)) # (!GLOBAL(\Mux16~0clkctrl_outclk\) & ((\Mux14~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \R[14]$latch~combout\,
	datac => \Mux14~3_combout\,
	datad => \Mux16~0clkctrl_outclk\,
	combout => \R[14]$latch~combout\);

-- Location: IOIBUF_X0_Y10_N1
\E[3][15]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(15),
	o => \E[3][15]~input_o\);

-- Location: IOIBUF_X0_Y23_N1
\E[0][15]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(15),
	o => \E[0][15]~input_o\);

-- Location: IOIBUF_X0_Y26_N15
\E[2][15]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(15),
	o => \E[2][15]~input_o\);

-- Location: LCCOMB_X1_Y26_N28
\Mux15~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux15~0_combout\ = (\S[1]~input_o\ & (((\E[2][15]~input_o\) # (\S[0]~input_o\)))) # (!\S[1]~input_o\ & (\E[0][15]~input_o\ & ((!\S[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[0][15]~input_o\,
	datab => \E[2][15]~input_o\,
	datac => \S[1]~input_o\,
	datad => \S[0]~input_o\,
	combout => \Mux15~0_combout\);

-- Location: IOIBUF_X0_Y26_N22
\E[1][15]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(15),
	o => \E[1][15]~input_o\);

-- Location: LCCOMB_X1_Y26_N30
\Mux15~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux15~1_combout\ = (\Mux15~0_combout\ & ((\E[3][15]~input_o\) # ((!\S[0]~input_o\)))) # (!\Mux15~0_combout\ & (((\S[0]~input_o\ & \E[1][15]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[3][15]~input_o\,
	datab => \Mux15~0_combout\,
	datac => \S[0]~input_o\,
	datad => \E[1][15]~input_o\,
	combout => \Mux15~1_combout\);

-- Location: IOIBUF_X0_Y26_N8
\E[4][15]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(15),
	o => \E[4][15]~input_o\);

-- Location: IOIBUF_X0_Y18_N1
\E[5][15]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(15),
	o => \E[5][15]~input_o\);

-- Location: LCCOMB_X1_Y26_N20
\Mux15~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux15~2_combout\ = (\Mux0~0_combout\ & (((\E[5][15]~input_o\) # (\Mux0~3_combout\)))) # (!\Mux0~0_combout\ & (\E[4][15]~input_o\ & ((!\Mux0~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[4][15]~input_o\,
	datab => \E[5][15]~input_o\,
	datac => \Mux0~0_combout\,
	datad => \Mux0~3_combout\,
	combout => \Mux15~2_combout\);

-- Location: IOIBUF_X0_Y23_N15
\E[6][15]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(15),
	o => \E[6][15]~input_o\);

-- Location: LCCOMB_X1_Y26_N6
\Mux15~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \Mux15~3_combout\ = (\Mux15~2_combout\ & (((\E[6][15]~input_o\) # (!\Mux0~3_combout\)))) # (!\Mux15~2_combout\ & (\Mux15~1_combout\ & ((\Mux0~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Mux15~1_combout\,
	datab => \Mux15~2_combout\,
	datac => \E[6][15]~input_o\,
	datad => \Mux0~3_combout\,
	combout => \Mux15~3_combout\);

-- Location: LCCOMB_X1_Y26_N14
\R[15]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[15]$latch~combout\ = (GLOBAL(\Mux16~0clkctrl_outclk\) & ((\R[15]$latch~combout\))) # (!GLOBAL(\Mux16~0clkctrl_outclk\) & (\Mux15~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Mux15~3_combout\,
	datac => \R[15]$latch~combout\,
	datad => \Mux16~0clkctrl_outclk\,
	combout => \R[15]$latch~combout\);

-- Location: UNVM_X0_Y40_N40
\~QUARTUS_CREATED_UNVM~\ : fiftyfivenm_unvm
-- pragma translate_off
GENERIC MAP (
	addr_range1_end_addr => -1,
	addr_range1_offset => -1,
	addr_range2_end_addr => -1,
	addr_range2_offset => -1,
	addr_range3_offset => -1,
	is_compressed_image => "false",
	is_dual_boot => "false",
	is_eram_skip => "false",
	max_ufm_valid_addr => -1,
	max_valid_addr => -1,
	min_ufm_valid_addr => -1,
	min_valid_addr => -1,
	part_name => "quartus_created_unvm",
	reserve_block => "true")
-- pragma translate_on
PORT MAP (
	nosc_ena => \~QUARTUS_CREATED_GND~I_combout\,
	xe_ye => \~QUARTUS_CREATED_GND~I_combout\,
	se => \~QUARTUS_CREATED_GND~I_combout\,
	busy => \~QUARTUS_CREATED_UNVM~~busy\);

-- Location: ADCBLOCK_X43_Y52_N0
\~QUARTUS_CREATED_ADC1~\ : fiftyfivenm_adcblock
-- pragma translate_off
GENERIC MAP (
	analog_input_pin_mask => 0,
	clkdiv => 1,
	device_partname_fivechar_prefix => "none",
	is_this_first_or_second_adc => 1,
	prescalar => 0,
	pwd => 1,
	refsel => 0,
	reserve_block => "true",
	testbits => 66,
	tsclkdiv => 1,
	tsclksel => 0)
-- pragma translate_on
PORT MAP (
	soc => \~QUARTUS_CREATED_GND~I_combout\,
	usr_pwd => VCC,
	tsen => \~QUARTUS_CREATED_GND~I_combout\,
	chsel => \~QUARTUS_CREATED_ADC1~_CHSEL_bus\,
	eoc => \~QUARTUS_CREATED_ADC1~~eoc\);

-- Location: ADCBLOCK_X43_Y51_N0
\~QUARTUS_CREATED_ADC2~\ : fiftyfivenm_adcblock
-- pragma translate_off
GENERIC MAP (
	analog_input_pin_mask => 0,
	clkdiv => 1,
	device_partname_fivechar_prefix => "none",
	is_this_first_or_second_adc => 2,
	prescalar => 0,
	pwd => 1,
	refsel => 0,
	reserve_block => "true",
	testbits => 66,
	tsclkdiv => 1,
	tsclksel => 0)
-- pragma translate_on
PORT MAP (
	soc => \~QUARTUS_CREATED_GND~I_combout\,
	usr_pwd => VCC,
	tsen => \~QUARTUS_CREATED_GND~I_combout\,
	chsel => \~QUARTUS_CREATED_ADC2~_CHSEL_bus\,
	eoc => \~QUARTUS_CREATED_ADC2~~eoc\);

ww_R(0) <= \R[0]~output_o\;

ww_R(1) <= \R[1]~output_o\;

ww_R(2) <= \R[2]~output_o\;

ww_R(3) <= \R[3]~output_o\;

ww_R(4) <= \R[4]~output_o\;

ww_R(5) <= \R[5]~output_o\;

ww_R(6) <= \R[6]~output_o\;

ww_R(7) <= \R[7]~output_o\;

ww_R(8) <= \R[8]~output_o\;

ww_R(9) <= \R[9]~output_o\;

ww_R(10) <= \R[10]~output_o\;

ww_R(11) <= \R[11]~output_o\;

ww_R(12) <= \R[12]~output_o\;

ww_R(13) <= \R[13]~output_o\;

ww_R(14) <= \R[14]~output_o\;

ww_R(15) <= \R[15]~output_o\;
END structure;



-- Copyright (C) 2021  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 21.1.0 Build 842 10/21/2021 SJ Lite Edition"

-- DATE "06/20/2022 00:13:09"

-- 
-- Device: Altera 10M50DAF484C7G Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_TMS~	=>  Location: PIN_H2,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TCK~	=>  Location: PIN_G2,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDI~	=>  Location: PIN_L4,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDO~	=>  Location: PIN_M5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_CONFIG_SEL~	=>  Location: PIN_H10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCONFIG~	=>  Location: PIN_H9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_nSTATUS~	=>  Location: PIN_G9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_CONF_DONE~	=>  Location: PIN_F8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_TMS~~padout\ : std_logic;
SIGNAL \~ALTERA_TCK~~padout\ : std_logic;
SIGNAL \~ALTERA_TDI~~padout\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~padout\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~padout\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~padout\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~padout\ : std_logic;
SIGNAL \~ALTERA_TMS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TCK~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TDI~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
LIBRARY WORK;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;
USE WORK.BUS_TRAIL.ALL;

ENTITY 	L2_3_1 IS
    PORT (
	E : IN WORK.BUS_TRAIL.bus_matrix;
	S : IN std_logic_vector(2 DOWNTO 0);
	R : OUT std_logic_vector(15 DOWNTO 0)
	);
END L2_3_1;

-- Design Ports Information
-- R[0]	=>  Location: PIN_G22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[1]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[2]	=>  Location: PIN_D9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[3]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[4]	=>  Location: PIN_N21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[5]	=>  Location: PIN_J12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[6]	=>  Location: PIN_D14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[7]	=>  Location: PIN_A10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[8]	=>  Location: PIN_B19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[9]	=>  Location: PIN_M15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[10]	=>  Location: PIN_P18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[11]	=>  Location: PIN_R18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[12]	=>  Location: PIN_E18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[13]	=>  Location: PIN_P19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[14]	=>  Location: PIN_C7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R[15]	=>  Location: PIN_N14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][0]	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[1]	=>  Location: PIN_L20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[0]	=>  Location: PIN_K19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S[2]	=>  Location: PIN_D19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][0]	=>  Location: PIN_B14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][0]	=>  Location: PIN_A16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][0]	=>  Location: PIN_J11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][0]	=>  Location: PIN_E13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][0]	=>  Location: PIN_D13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][0]	=>  Location: PIN_A8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][1]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][1]	=>  Location: PIN_E10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][1]	=>  Location: PIN_E16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][1]	=>  Location: PIN_B15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][1]	=>  Location: PIN_D15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][1]	=>  Location: PIN_B10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][1]	=>  Location: PIN_C14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][2]	=>  Location: PIN_B12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][2]	=>  Location: PIN_C11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][2]	=>  Location: PIN_A15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][2]	=>  Location: PIN_D17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][2]	=>  Location: PIN_J13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][2]	=>  Location: PIN_C10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][2]	=>  Location: PIN_H14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][3]	=>  Location: PIN_A20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][3]	=>  Location: PIN_C16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][3]	=>  Location: PIN_E14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][3]	=>  Location: PIN_C17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][3]	=>  Location: PIN_A18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][3]	=>  Location: PIN_A5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][3]	=>  Location: PIN_B16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][4]	=>  Location: PIN_C15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][4]	=>  Location: PIN_A11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][4]	=>  Location: PIN_E12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][4]	=>  Location: PIN_K20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][4]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][4]	=>  Location: PIN_K18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][4]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][5]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][5]	=>  Location: PIN_L19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][5]	=>  Location: PIN_B17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][5]	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][5]	=>  Location: PIN_B21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][5]	=>  Location: PIN_H11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][5]	=>  Location: PIN_F17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][6]	=>  Location: PIN_C22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][6]	=>  Location: PIN_H19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][6]	=>  Location: PIN_J20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][6]	=>  Location: PIN_C18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][6]	=>  Location: PIN_B22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][6]	=>  Location: PIN_F16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][6]	=>  Location: PIN_E17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][7]	=>  Location: PIN_B11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][7]	=>  Location: PIN_H18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][7]	=>  Location: PIN_D18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][7]	=>  Location: PIN_J14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][7]	=>  Location: PIN_C19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][7]	=>  Location: PIN_A12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][7]	=>  Location: PIN_H20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][8]	=>  Location: PIN_A19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][8]	=>  Location: PIN_H13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][8]	=>  Location: PIN_C8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][8]	=>  Location: PIN_C9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][8]	=>  Location: PIN_A9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][8]	=>  Location: PIN_A14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][8]	=>  Location: PIN_B8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][9]	=>  Location: PIN_K15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][9]	=>  Location: PIN_A7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][9]	=>  Location: PIN_L18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][9]	=>  Location: PIN_J22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][9]	=>  Location: PIN_K14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][9]	=>  Location: PIN_J10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][9]	=>  Location: PIN_C20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][10]	=>  Location: PIN_F22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][10]	=>  Location: PIN_J15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][10]	=>  Location: PIN_P21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][10]	=>  Location: PIN_N19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][10]	=>  Location: PIN_N18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][10]	=>  Location: PIN_F21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][10]	=>  Location: PIN_M18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][11]	=>  Location: PIN_G19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][11]	=>  Location: PIN_J18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][11]	=>  Location: PIN_M20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][11]	=>  Location: PIN_D22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][11]	=>  Location: PIN_H17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][11]	=>  Location: PIN_G17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][11]	=>  Location: PIN_F20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][12]	=>  Location: PIN_N20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][12]	=>  Location: PIN_K22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][12]	=>  Location: PIN_J21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][12]	=>  Location: PIN_B20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][12]	=>  Location: PIN_H12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][12]	=>  Location: PIN_A21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][12]	=>  Location: PIN_M22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][13]	=>  Location: PIN_D21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][13]	=>  Location: PIN_L14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][13]	=>  Location: PIN_N15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][13]	=>  Location: PIN_E19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][13]	=>  Location: PIN_L22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][13]	=>  Location: PIN_H22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][13]	=>  Location: PIN_K21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][14]	=>  Location: PIN_B7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][14]	=>  Location: PIN_E21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][14]	=>  Location: PIN_E20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][14]	=>  Location: PIN_N22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][14]	=>  Location: PIN_M14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][14]	=>  Location: PIN_P20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][14]	=>  Location: PIN_L15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[1][15]	=>  Location: PIN_F19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[4][15]	=>  Location: PIN_A6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[0][15]	=>  Location: PIN_C21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[5][15]	=>  Location: PIN_F18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[3][15]	=>  Location: PIN_G20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[2][15]	=>  Location: PIN_H21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E[6][15]	=>  Location: PIN_E22,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF L2_3_1 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_E : WORK.BUS_TRAIL.bus_matrix;
SIGNAL ww_S : std_logic_vector(2 DOWNTO 0);
SIGNAL ww_R : std_logic_vector(15 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_ADC1~_CHSEL_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_ADC2~_CHSEL_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \R[15]~6clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \~QUARTUS_CREATED_UNVM~~busy\ : std_logic;
SIGNAL \~QUARTUS_CREATED_ADC1~~eoc\ : std_logic;
SIGNAL \~QUARTUS_CREATED_ADC2~~eoc\ : std_logic;
SIGNAL \R[0]~output_o\ : std_logic;
SIGNAL \R[1]~output_o\ : std_logic;
SIGNAL \R[2]~output_o\ : std_logic;
SIGNAL \R[3]~output_o\ : std_logic;
SIGNAL \R[4]~output_o\ : std_logic;
SIGNAL \R[5]~output_o\ : std_logic;
SIGNAL \R[6]~output_o\ : std_logic;
SIGNAL \R[7]~output_o\ : std_logic;
SIGNAL \R[8]~output_o\ : std_logic;
SIGNAL \R[9]~output_o\ : std_logic;
SIGNAL \R[10]~output_o\ : std_logic;
SIGNAL \R[11]~output_o\ : std_logic;
SIGNAL \R[12]~output_o\ : std_logic;
SIGNAL \R[13]~output_o\ : std_logic;
SIGNAL \R[14]~output_o\ : std_logic;
SIGNAL \R[15]~output_o\ : std_logic;
SIGNAL \E[6][0]~input_o\ : std_logic;
SIGNAL \S[0]~input_o\ : std_logic;
SIGNAL \S[2]~input_o\ : std_logic;
SIGNAL \S[1]~input_o\ : std_logic;
SIGNAL \R[0]~0_combout\ : std_logic;
SIGNAL \R[0]~3_combout\ : std_logic;
SIGNAL \E[1][0]~input_o\ : std_logic;
SIGNAL \E[0][0]~input_o\ : std_logic;
SIGNAL \E[4][0]~input_o\ : std_logic;
SIGNAL \R[0]~1_combout\ : std_logic;
SIGNAL \E[5][0]~input_o\ : std_logic;
SIGNAL \R[0]~2_combout\ : std_logic;
SIGNAL \E[2][0]~input_o\ : std_logic;
SIGNAL \R[0]~4_combout\ : std_logic;
SIGNAL \E[3][0]~input_o\ : std_logic;
SIGNAL \R[0]~5_combout\ : std_logic;
SIGNAL \R[15]~6_combout\ : std_logic;
SIGNAL \R[15]~6clkctrl_outclk\ : std_logic;
SIGNAL \R[0]$latch~combout\ : std_logic;
SIGNAL \E[2][1]~input_o\ : std_logic;
SIGNAL \E[3][1]~input_o\ : std_logic;
SIGNAL \R[1]~9_combout\ : std_logic;
SIGNAL \E[0][1]~input_o\ : std_logic;
SIGNAL \E[4][1]~input_o\ : std_logic;
SIGNAL \R[1]~7_combout\ : std_logic;
SIGNAL \E[5][1]~input_o\ : std_logic;
SIGNAL \E[1][1]~input_o\ : std_logic;
SIGNAL \R[1]~8_combout\ : std_logic;
SIGNAL \E[6][1]~input_o\ : std_logic;
SIGNAL \R[1]~10_combout\ : std_logic;
SIGNAL \R[1]$latch~combout\ : std_logic;
SIGNAL \E[3][2]~input_o\ : std_logic;
SIGNAL \E[5][2]~input_o\ : std_logic;
SIGNAL \E[1][2]~input_o\ : std_logic;
SIGNAL \E[0][2]~input_o\ : std_logic;
SIGNAL \R[2]~11_combout\ : std_logic;
SIGNAL \E[4][2]~input_o\ : std_logic;
SIGNAL \R[2]~12_combout\ : std_logic;
SIGNAL \E[2][2]~input_o\ : std_logic;
SIGNAL \R[2]~13_combout\ : std_logic;
SIGNAL \E[6][2]~input_o\ : std_logic;
SIGNAL \R[2]~14_combout\ : std_logic;
SIGNAL \R[2]$latch~combout\ : std_logic;
SIGNAL \E[3][3]~input_o\ : std_logic;
SIGNAL \E[2][3]~input_o\ : std_logic;
SIGNAL \R[3]~17_combout\ : std_logic;
SIGNAL \E[5][3]~input_o\ : std_logic;
SIGNAL \E[0][3]~input_o\ : std_logic;
SIGNAL \E[4][3]~input_o\ : std_logic;
SIGNAL \R[3]~15_combout\ : std_logic;
SIGNAL \E[1][3]~input_o\ : std_logic;
SIGNAL \R[3]~16_combout\ : std_logic;
SIGNAL \E[6][3]~input_o\ : std_logic;
SIGNAL \R[3]~18_combout\ : std_logic;
SIGNAL \R[3]$latch~combout\ : std_logic;
SIGNAL \E[6][4]~input_o\ : std_logic;
SIGNAL \E[0][4]~input_o\ : std_logic;
SIGNAL \E[1][4]~input_o\ : std_logic;
SIGNAL \R[4]~19_combout\ : std_logic;
SIGNAL \E[4][4]~input_o\ : std_logic;
SIGNAL \E[5][4]~input_o\ : std_logic;
SIGNAL \R[4]~20_combout\ : std_logic;
SIGNAL \E[2][4]~input_o\ : std_logic;
SIGNAL \R[4]~21_combout\ : std_logic;
SIGNAL \E[3][4]~input_o\ : std_logic;
SIGNAL \R[4]~22_combout\ : std_logic;
SIGNAL \R[4]$latch~combout\ : std_logic;
SIGNAL \E[3][5]~input_o\ : std_logic;
SIGNAL \E[2][5]~input_o\ : std_logic;
SIGNAL \R[5]~25_combout\ : std_logic;
SIGNAL \E[4][5]~input_o\ : std_logic;
SIGNAL \E[0][5]~input_o\ : std_logic;
SIGNAL \R[5]~23_combout\ : std_logic;
SIGNAL \E[1][5]~input_o\ : std_logic;
SIGNAL \E[5][5]~input_o\ : std_logic;
SIGNAL \R[5]~24_combout\ : std_logic;
SIGNAL \E[6][5]~input_o\ : std_logic;
SIGNAL \R[5]~26_combout\ : std_logic;
SIGNAL \R[5]$latch~combout\ : std_logic;
SIGNAL \E[3][6]~input_o\ : std_logic;
SIGNAL \E[1][6]~input_o\ : std_logic;
SIGNAL \E[0][6]~input_o\ : std_logic;
SIGNAL \R[6]~27_combout\ : std_logic;
SIGNAL \E[5][6]~input_o\ : std_logic;
SIGNAL \E[4][6]~input_o\ : std_logic;
SIGNAL \R[6]~28_combout\ : std_logic;
SIGNAL \E[2][6]~input_o\ : std_logic;
SIGNAL \R[6]~29_combout\ : std_logic;
SIGNAL \E[6][6]~input_o\ : std_logic;
SIGNAL \R[6]~30_combout\ : std_logic;
SIGNAL \R[6]$latch~combout\ : std_logic;
SIGNAL \E[6][7]~input_o\ : std_logic;
SIGNAL \E[3][7]~input_o\ : std_logic;
SIGNAL \E[2][7]~input_o\ : std_logic;
SIGNAL \R[7]~33_combout\ : std_logic;
SIGNAL \E[5][7]~input_o\ : std_logic;
SIGNAL \E[4][7]~input_o\ : std_logic;
SIGNAL \E[0][7]~input_o\ : std_logic;
SIGNAL \R[7]~31_combout\ : std_logic;
SIGNAL \E[1][7]~input_o\ : std_logic;
SIGNAL \R[7]~32_combout\ : std_logic;
SIGNAL \R[7]~34_combout\ : std_logic;
SIGNAL \R[7]$latch~combout\ : std_logic;
SIGNAL \E[1][8]~input_o\ : std_logic;
SIGNAL \E[0][8]~input_o\ : std_logic;
SIGNAL \R[8]~35_combout\ : std_logic;
SIGNAL \E[5][8]~input_o\ : std_logic;
SIGNAL \E[4][8]~input_o\ : std_logic;
SIGNAL \R[8]~36_combout\ : std_logic;
SIGNAL \E[2][8]~input_o\ : std_logic;
SIGNAL \R[8]~37_combout\ : std_logic;
SIGNAL \E[3][8]~input_o\ : std_logic;
SIGNAL \E[6][8]~input_o\ : std_logic;
SIGNAL \R[8]~38_combout\ : std_logic;
SIGNAL \R[8]$latch~combout\ : std_logic;
SIGNAL \E[6][9]~input_o\ : std_logic;
SIGNAL \E[3][9]~input_o\ : std_logic;
SIGNAL \E[2][9]~input_o\ : std_logic;
SIGNAL \R[9]~41_combout\ : std_logic;
SIGNAL \E[1][9]~input_o\ : std_logic;
SIGNAL \E[5][9]~input_o\ : std_logic;
SIGNAL \E[4][9]~input_o\ : std_logic;
SIGNAL \E[0][9]~input_o\ : std_logic;
SIGNAL \R[9]~39_combout\ : std_logic;
SIGNAL \R[9]~40_combout\ : std_logic;
SIGNAL \R[9]~42_combout\ : std_logic;
SIGNAL \R[9]$latch~combout\ : std_logic;
SIGNAL \E[4][10]~input_o\ : std_logic;
SIGNAL \E[1][10]~input_o\ : std_logic;
SIGNAL \E[0][10]~input_o\ : std_logic;
SIGNAL \R[10]~43_combout\ : std_logic;
SIGNAL \E[5][10]~input_o\ : std_logic;
SIGNAL \R[10]~44_combout\ : std_logic;
SIGNAL \E[2][10]~input_o\ : std_logic;
SIGNAL \R[10]~45_combout\ : std_logic;
SIGNAL \E[3][10]~input_o\ : std_logic;
SIGNAL \E[6][10]~input_o\ : std_logic;
SIGNAL \R[10]~46_combout\ : std_logic;
SIGNAL \R[10]$latch~combout\ : std_logic;
SIGNAL \E[6][11]~input_o\ : std_logic;
SIGNAL \E[2][11]~input_o\ : std_logic;
SIGNAL \E[3][11]~input_o\ : std_logic;
SIGNAL \R[11]~49_combout\ : std_logic;
SIGNAL \E[1][11]~input_o\ : std_logic;
SIGNAL \E[5][11]~input_o\ : std_logic;
SIGNAL \E[4][11]~input_o\ : std_logic;
SIGNAL \E[0][11]~input_o\ : std_logic;
SIGNAL \R[11]~47_combout\ : std_logic;
SIGNAL \R[11]~48_combout\ : std_logic;
SIGNAL \R[11]~50_combout\ : std_logic;
SIGNAL \R[11]$latch~combout\ : std_logic;
SIGNAL \E[5][12]~input_o\ : std_logic;
SIGNAL \E[1][12]~input_o\ : std_logic;
SIGNAL \E[0][12]~input_o\ : std_logic;
SIGNAL \R[12]~51_combout\ : std_logic;
SIGNAL \E[4][12]~input_o\ : std_logic;
SIGNAL \R[12]~52_combout\ : std_logic;
SIGNAL \E[2][12]~input_o\ : std_logic;
SIGNAL \R[12]~53_combout\ : std_logic;
SIGNAL \E[3][12]~input_o\ : std_logic;
SIGNAL \E[6][12]~input_o\ : std_logic;
SIGNAL \R[12]~54_combout\ : std_logic;
SIGNAL \R[12]$latch~combout\ : std_logic;
SIGNAL \E[3][13]~input_o\ : std_logic;
SIGNAL \E[2][13]~input_o\ : std_logic;
SIGNAL \R[13]~57_combout\ : std_logic;
SIGNAL \E[0][13]~input_o\ : std_logic;
SIGNAL \E[4][13]~input_o\ : std_logic;
SIGNAL \R[13]~55_combout\ : std_logic;
SIGNAL \E[1][13]~input_o\ : std_logic;
SIGNAL \E[5][13]~input_o\ : std_logic;
SIGNAL \R[13]~56_combout\ : std_logic;
SIGNAL \E[6][13]~input_o\ : std_logic;
SIGNAL \R[13]~58_combout\ : std_logic;
SIGNAL \R[13]$latch~combout\ : std_logic;
SIGNAL \E[3][14]~input_o\ : std_logic;
SIGNAL \E[6][14]~input_o\ : std_logic;
SIGNAL \E[2][14]~input_o\ : std_logic;
SIGNAL \E[5][14]~input_o\ : std_logic;
SIGNAL \E[1][14]~input_o\ : std_logic;
SIGNAL \E[0][14]~input_o\ : std_logic;
SIGNAL \E[4][14]~input_o\ : std_logic;
SIGNAL \R[14]~59_combout\ : std_logic;
SIGNAL \R[14]~60_combout\ : std_logic;
SIGNAL \R[14]~61_combout\ : std_logic;
SIGNAL \R[14]~62_combout\ : std_logic;
SIGNAL \R[14]$latch~combout\ : std_logic;
SIGNAL \E[6][15]~input_o\ : std_logic;
SIGNAL \E[5][15]~input_o\ : std_logic;
SIGNAL \E[0][15]~input_o\ : std_logic;
SIGNAL \E[4][15]~input_o\ : std_logic;
SIGNAL \R[15]~63_combout\ : std_logic;
SIGNAL \E[1][15]~input_o\ : std_logic;
SIGNAL \R[15]~64_combout\ : std_logic;
SIGNAL \E[2][15]~input_o\ : std_logic;
SIGNAL \E[3][15]~input_o\ : std_logic;
SIGNAL \R[15]~65_combout\ : std_logic;
SIGNAL \R[15]~66_combout\ : std_logic;
SIGNAL \R[15]$latch~combout\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_E <= E;
ww_S <= S;
R <= ww_R;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\~QUARTUS_CREATED_ADC1~_CHSEL_bus\ <= (\~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\);

\~QUARTUS_CREATED_ADC2~_CHSEL_bus\ <= (\~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\);

\R[15]~6clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \R[15]~6_combout\);
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: LCCOMB_X44_Y43_N16
\~QUARTUS_CREATED_GND~I\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \~QUARTUS_CREATED_GND~I_combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~QUARTUS_CREATED_GND~I_combout\);

-- Location: IOOBUF_X78_Y31_N9
\R[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[0]$latch~combout\,
	devoe => ww_devoe,
	o => \R[0]~output_o\);

-- Location: IOOBUF_X54_Y54_N16
\R[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[1]$latch~combout\,
	devoe => ww_devoe,
	o => \R[1]~output_o\);

-- Location: IOOBUF_X31_Y39_N9
\R[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[2]$latch~combout\,
	devoe => ww_devoe,
	o => \R[2]~output_o\);

-- Location: IOOBUF_X69_Y54_N2
\R[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[3]$latch~combout\,
	devoe => ww_devoe,
	o => \R[3]~output_o\);

-- Location: IOOBUF_X78_Y25_N9
\R[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[4]$latch~combout\,
	devoe => ww_devoe,
	o => \R[4]~output_o\);

-- Location: IOOBUF_X54_Y54_N9
\R[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[5]$latch~combout\,
	devoe => ww_devoe,
	o => \R[5]~output_o\);

-- Location: IOOBUF_X56_Y54_N9
\R[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[6]$latch~combout\,
	devoe => ww_devoe,
	o => \R[6]~output_o\);

-- Location: IOOBUF_X51_Y54_N16
\R[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[7]$latch~combout\,
	devoe => ww_devoe,
	o => \R[7]~output_o\);

-- Location: IOOBUF_X69_Y54_N16
\R[8]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[8]$latch~combout\,
	devoe => ww_devoe,
	o => \R[8]~output_o\);

-- Location: IOOBUF_X78_Y33_N23
\R[9]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[9]$latch~combout\,
	devoe => ww_devoe,
	o => \R[9]~output_o\);

-- Location: IOOBUF_X78_Y24_N16
\R[10]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[10]$latch~combout\,
	devoe => ww_devoe,
	o => \R[10]~output_o\);

-- Location: IOOBUF_X78_Y24_N24
\R[11]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[11]$latch~combout\,
	devoe => ww_devoe,
	o => \R[11]~output_o\);

-- Location: IOOBUF_X78_Y49_N2
\R[12]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[12]$latch~combout\,
	devoe => ww_devoe,
	o => \R[12]~output_o\);

-- Location: IOOBUF_X78_Y24_N9
\R[13]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[13]$latch~combout\,
	devoe => ww_devoe,
	o => \R[13]~output_o\);

-- Location: IOOBUF_X34_Y39_N2
\R[14]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[14]$latch~combout\,
	devoe => ww_devoe,
	o => \R[14]~output_o\);

-- Location: IOOBUF_X78_Y29_N23
\R[15]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \R[15]$latch~combout\,
	devoe => ww_devoe,
	o => \R[15]~output_o\);

-- Location: IOIBUF_X46_Y54_N1
\E[6][0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(0),
	o => \E[6][0]~input_o\);

-- Location: IOIBUF_X78_Y42_N8
\S[0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S(0),
	o => \S[0]~input_o\);

-- Location: IOIBUF_X78_Y41_N1
\S[2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S(2),
	o => \S[2]~input_o\);

-- Location: IOIBUF_X78_Y37_N1
\S[1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S(1),
	o => \S[1]~input_o\);

-- Location: LCCOMB_X77_Y42_N20
\R[0]~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[0]~0_combout\ = (\S[1]~input_o\ & ((\S[0]~input_o\) # (\S[2]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \S[2]~input_o\,
	datac => \S[1]~input_o\,
	combout => \R[0]~0_combout\);

-- Location: LCCOMB_X77_Y39_N0
\R[0]~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[0]~3_combout\ = (\S[2]~input_o\) # (!\S[1]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \S[1]~input_o\,
	datad => \S[2]~input_o\,
	combout => \R[0]~3_combout\);

-- Location: IOIBUF_X56_Y54_N1
\E[1][0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(0),
	o => \E[1][0]~input_o\);

-- Location: IOIBUF_X49_Y54_N22
\E[0][0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(0),
	o => \E[0][0]~input_o\);

-- Location: IOIBUF_X60_Y54_N15
\E[4][0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(0),
	o => \E[4][0]~input_o\);

-- Location: LCCOMB_X75_Y42_N20
\R[0]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[0]~1_combout\ = (\S[0]~input_o\ & (((\S[2]~input_o\)))) # (!\S[0]~input_o\ & ((\S[2]~input_o\ & ((\E[4][0]~input_o\))) # (!\S[2]~input_o\ & (\E[0][0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[0][0]~input_o\,
	datab => \E[4][0]~input_o\,
	datac => \S[0]~input_o\,
	datad => \S[2]~input_o\,
	combout => \R[0]~1_combout\);

-- Location: IOIBUF_X56_Y54_N15
\E[5][0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(0),
	o => \E[5][0]~input_o\);

-- Location: LCCOMB_X75_Y42_N18
\R[0]~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[0]~2_combout\ = (\S[0]~input_o\ & ((\R[0]~1_combout\ & ((\E[5][0]~input_o\))) # (!\R[0]~1_combout\ & (\E[1][0]~input_o\)))) # (!\S[0]~input_o\ & (((\R[0]~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100001011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \E[1][0]~input_o\,
	datac => \R[0]~1_combout\,
	datad => \E[5][0]~input_o\,
	combout => \R[0]~2_combout\);

-- Location: IOIBUF_X56_Y54_N29
\E[2][0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(0),
	o => \E[2][0]~input_o\);

-- Location: LCCOMB_X75_Y42_N28
\R[0]~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[0]~4_combout\ = (\R[0]~0_combout\ & (\R[0]~3_combout\)) # (!\R[0]~0_combout\ & ((\R[0]~3_combout\ & (\R[0]~2_combout\)) # (!\R[0]~3_combout\ & ((\E[2][0]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[0]~0_combout\,
	datab => \R[0]~3_combout\,
	datac => \R[0]~2_combout\,
	datad => \E[2][0]~input_o\,
	combout => \R[0]~4_combout\);

-- Location: IOIBUF_X74_Y54_N8
\E[3][0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(0),
	o => \E[3][0]~input_o\);

-- Location: LCCOMB_X75_Y42_N6
\R[0]~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[0]~5_combout\ = (\R[0]~4_combout\ & ((\E[6][0]~input_o\) # ((!\R[0]~0_combout\)))) # (!\R[0]~4_combout\ & (((\R[0]~0_combout\ & \E[3][0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[6][0]~input_o\,
	datab => \R[0]~4_combout\,
	datac => \R[0]~0_combout\,
	datad => \E[3][0]~input_o\,
	combout => \R[0]~5_combout\);

-- Location: LCCOMB_X77_Y39_N16
\R[15]~6\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[15]~6_combout\ = ((!\S[0]~input_o\) # (!\S[1]~input_o\)) # (!\S[2]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[2]~input_o\,
	datac => \S[1]~input_o\,
	datad => \S[0]~input_o\,
	combout => \R[15]~6_combout\);

-- Location: CLKCTRL_G8
\R[15]~6clkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \R[15]~6clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \R[15]~6clkctrl_outclk\);

-- Location: LCCOMB_X75_Y42_N4
\R[0]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[0]$latch~combout\ = (GLOBAL(\R[15]~6clkctrl_outclk\) & (\R[0]~5_combout\)) # (!GLOBAL(\R[15]~6clkctrl_outclk\) & ((\R[0]$latch~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \R[0]~5_combout\,
	datac => \R[0]$latch~combout\,
	datad => \R[15]~6clkctrl_outclk\,
	combout => \R[0]$latch~combout\);

-- Location: IOIBUF_X46_Y54_N8
\E[2][1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(1),
	o => \E[2][1]~input_o\);

-- Location: IOIBUF_X66_Y54_N15
\E[3][1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(1),
	o => \E[3][1]~input_o\);

-- Location: LCCOMB_X74_Y42_N16
\R[1]~9\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[1]~9_combout\ = (\R[0]~3_combout\ & (((\R[0]~0_combout\)))) # (!\R[0]~3_combout\ & ((\R[0]~0_combout\ & ((\E[3][1]~input_o\))) # (!\R[0]~0_combout\ & (\E[2][1]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[0]~3_combout\,
	datab => \E[2][1]~input_o\,
	datac => \E[3][1]~input_o\,
	datad => \R[0]~0_combout\,
	combout => \R[1]~9_combout\);

-- Location: IOIBUF_X74_Y54_N1
\E[0][1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(1),
	o => \E[0][1]~input_o\);

-- Location: IOIBUF_X36_Y39_N22
\E[4][1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(1),
	o => \E[4][1]~input_o\);

-- Location: LCCOMB_X74_Y42_N20
\R[1]~7\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[1]~7_combout\ = (\S[2]~input_o\ & (((\S[0]~input_o\) # (\E[4][1]~input_o\)))) # (!\S[2]~input_o\ & (\E[0][1]~input_o\ & (!\S[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[2]~input_o\,
	datab => \E[0][1]~input_o\,
	datac => \S[0]~input_o\,
	datad => \E[4][1]~input_o\,
	combout => \R[1]~7_combout\);

-- Location: IOIBUF_X58_Y54_N8
\E[5][1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(1),
	o => \E[5][1]~input_o\);

-- Location: IOIBUF_X58_Y54_N22
\E[1][1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(1),
	o => \E[1][1]~input_o\);

-- Location: LCCOMB_X74_Y42_N18
\R[1]~8\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[1]~8_combout\ = (\R[1]~7_combout\ & (((\E[5][1]~input_o\)) # (!\S[0]~input_o\))) # (!\R[1]~7_combout\ & (\S[0]~input_o\ & ((\E[1][1]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110011010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[1]~7_combout\,
	datab => \S[0]~input_o\,
	datac => \E[5][1]~input_o\,
	datad => \E[1][1]~input_o\,
	combout => \R[1]~8_combout\);

-- Location: IOIBUF_X58_Y54_N15
\E[6][1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(1),
	o => \E[6][1]~input_o\);

-- Location: LCCOMB_X74_Y42_N2
\R[1]~10\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[1]~10_combout\ = (\R[0]~3_combout\ & ((\R[1]~9_combout\ & ((\E[6][1]~input_o\))) # (!\R[1]~9_combout\ & (\R[1]~8_combout\)))) # (!\R[0]~3_combout\ & (\R[1]~9_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[0]~3_combout\,
	datab => \R[1]~9_combout\,
	datac => \R[1]~8_combout\,
	datad => \E[6][1]~input_o\,
	combout => \R[1]~10_combout\);

-- Location: LCCOMB_X74_Y42_N8
\R[1]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[1]$latch~combout\ = (GLOBAL(\R[15]~6clkctrl_outclk\) & (\R[1]~10_combout\)) # (!GLOBAL(\R[15]~6clkctrl_outclk\) & ((\R[1]$latch~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[1]~10_combout\,
	datac => \R[1]$latch~combout\,
	datad => \R[15]~6clkctrl_outclk\,
	combout => \R[1]$latch~combout\);

-- Location: IOIBUF_X49_Y54_N1
\E[3][2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(2),
	o => \E[3][2]~input_o\);

-- Location: IOIBUF_X60_Y54_N29
\E[5][2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(2),
	o => \E[5][2]~input_o\);

-- Location: IOIBUF_X58_Y54_N1
\E[1][2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(2),
	o => \E[1][2]~input_o\);

-- Location: IOIBUF_X74_Y54_N15
\E[0][2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(2),
	o => \E[0][2]~input_o\);

-- Location: LCCOMB_X75_Y42_N16
\R[2]~11\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[2]~11_combout\ = (\S[0]~input_o\ & ((\E[1][2]~input_o\) # ((\S[2]~input_o\)))) # (!\S[0]~input_o\ & (((\E[0][2]~input_o\ & !\S[2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \E[1][2]~input_o\,
	datac => \E[0][2]~input_o\,
	datad => \S[2]~input_o\,
	combout => \R[2]~11_combout\);

-- Location: IOIBUF_X51_Y54_N22
\E[4][2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(2),
	o => \E[4][2]~input_o\);

-- Location: LCCOMB_X75_Y42_N2
\R[2]~12\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[2]~12_combout\ = (\R[2]~11_combout\ & ((\E[5][2]~input_o\) # ((!\S[2]~input_o\)))) # (!\R[2]~11_combout\ & (((\E[4][2]~input_o\ & \S[2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[5][2]~input_o\,
	datab => \R[2]~11_combout\,
	datac => \E[4][2]~input_o\,
	datad => \S[2]~input_o\,
	combout => \R[2]~12_combout\);

-- Location: IOIBUF_X51_Y54_N29
\E[2][2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(2),
	o => \E[2][2]~input_o\);

-- Location: LCCOMB_X75_Y42_N12
\R[2]~13\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[2]~13_combout\ = (\R[0]~0_combout\ & (((\R[0]~3_combout\)))) # (!\R[0]~0_combout\ & ((\R[0]~3_combout\ & (\R[2]~12_combout\)) # (!\R[0]~3_combout\ & ((\E[2][2]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[0]~0_combout\,
	datab => \R[2]~12_combout\,
	datac => \R[0]~3_combout\,
	datad => \E[2][2]~input_o\,
	combout => \R[2]~13_combout\);

-- Location: IOIBUF_X60_Y54_N22
\E[6][2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(2),
	o => \E[6][2]~input_o\);

-- Location: LCCOMB_X75_Y42_N26
\R[2]~14\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[2]~14_combout\ = (\R[2]~13_combout\ & (((\E[6][2]~input_o\) # (!\R[0]~0_combout\)))) # (!\R[2]~13_combout\ & (\E[3][2]~input_o\ & (\R[0]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[3][2]~input_o\,
	datab => \R[2]~13_combout\,
	datac => \R[0]~0_combout\,
	datad => \E[6][2]~input_o\,
	combout => \R[2]~14_combout\);

-- Location: LCCOMB_X75_Y42_N30
\R[2]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[2]$latch~combout\ = (GLOBAL(\R[15]~6clkctrl_outclk\) & (\R[2]~14_combout\)) # (!GLOBAL(\R[15]~6clkctrl_outclk\) & ((\R[2]$latch~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \R[2]~14_combout\,
	datac => \R[2]$latch~combout\,
	datad => \R[15]~6clkctrl_outclk\,
	combout => \R[2]$latch~combout\);

-- Location: IOIBUF_X66_Y54_N29
\E[3][3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(3),
	o => \E[3][3]~input_o\);

-- Location: IOIBUF_X31_Y39_N15
\E[2][3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(3),
	o => \E[2][3]~input_o\);

-- Location: LCCOMB_X74_Y42_N24
\R[3]~17\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[3]~17_combout\ = (\R[0]~3_combout\ & (((\R[0]~0_combout\)))) # (!\R[0]~3_combout\ & ((\R[0]~0_combout\ & (\E[3][3]~input_o\)) # (!\R[0]~0_combout\ & ((\E[2][3]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[0]~3_combout\,
	datab => \E[3][3]~input_o\,
	datac => \E[2][3]~input_o\,
	datad => \R[0]~0_combout\,
	combout => \R[3]~17_combout\);

-- Location: IOIBUF_X74_Y54_N22
\E[5][3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(3),
	o => \E[5][3]~input_o\);

-- Location: IOIBUF_X66_Y54_N22
\E[0][3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(3),
	o => \E[0][3]~input_o\);

-- Location: IOIBUF_X62_Y54_N29
\E[4][3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(3),
	o => \E[4][3]~input_o\);

-- Location: LCCOMB_X74_Y42_N0
\R[3]~15\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[3]~15_combout\ = (\S[2]~input_o\ & (((\S[0]~input_o\) # (\E[4][3]~input_o\)))) # (!\S[2]~input_o\ & (\E[0][3]~input_o\ & (!\S[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111010100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[2]~input_o\,
	datab => \E[0][3]~input_o\,
	datac => \S[0]~input_o\,
	datad => \E[4][3]~input_o\,
	combout => \R[3]~15_combout\);

-- Location: IOIBUF_X66_Y54_N1
\E[1][3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(3),
	o => \E[1][3]~input_o\);

-- Location: LCCOMB_X74_Y42_N22
\R[3]~16\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[3]~16_combout\ = (\S[0]~input_o\ & ((\R[3]~15_combout\ & (\E[5][3]~input_o\)) # (!\R[3]~15_combout\ & ((\E[1][3]~input_o\))))) # (!\S[0]~input_o\ & (((\R[3]~15_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[5][3]~input_o\,
	datab => \S[0]~input_o\,
	datac => \R[3]~15_combout\,
	datad => \E[1][3]~input_o\,
	combout => \R[3]~16_combout\);

-- Location: IOIBUF_X60_Y54_N8
\E[6][3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(3),
	o => \E[6][3]~input_o\);

-- Location: LCCOMB_X74_Y42_N26
\R[3]~18\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[3]~18_combout\ = (\R[0]~3_combout\ & ((\R[3]~17_combout\ & ((\E[6][3]~input_o\))) # (!\R[3]~17_combout\ & (\R[3]~16_combout\)))) # (!\R[0]~3_combout\ & (\R[3]~17_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[0]~3_combout\,
	datab => \R[3]~17_combout\,
	datac => \R[3]~16_combout\,
	datad => \E[6][3]~input_o\,
	combout => \R[3]~18_combout\);

-- Location: LCCOMB_X74_Y42_N14
\R[3]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[3]$latch~combout\ = (GLOBAL(\R[15]~6clkctrl_outclk\) & (\R[3]~18_combout\)) # (!GLOBAL(\R[15]~6clkctrl_outclk\) & ((\R[3]$latch~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[3]~18_combout\,
	datac => \R[3]$latch~combout\,
	datad => \R[15]~6clkctrl_outclk\,
	combout => \R[3]$latch~combout\);

-- Location: IOIBUF_X51_Y54_N1
\E[6][4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(4),
	o => \E[6][4]~input_o\);

-- Location: IOIBUF_X78_Y42_N1
\E[0][4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(4),
	o => \E[0][4]~input_o\);

-- Location: IOIBUF_X56_Y54_N22
\E[1][4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(4),
	o => \E[1][4]~input_o\);

-- Location: LCCOMB_X75_Y42_N24
\R[4]~19\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[4]~19_combout\ = (\S[0]~input_o\ & ((\S[2]~input_o\) # ((\E[1][4]~input_o\)))) # (!\S[0]~input_o\ & (!\S[2]~input_o\ & (\E[0][4]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101010011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \S[2]~input_o\,
	datac => \E[0][4]~input_o\,
	datad => \E[1][4]~input_o\,
	combout => \R[4]~19_combout\);

-- Location: IOIBUF_X51_Y54_N8
\E[4][4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(4),
	o => \E[4][4]~input_o\);

-- Location: IOIBUF_X64_Y54_N1
\E[5][4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(4),
	o => \E[5][4]~input_o\);

-- Location: LCCOMB_X75_Y42_N10
\R[4]~20\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[4]~20_combout\ = (\R[4]~19_combout\ & (((\E[5][4]~input_o\) # (!\S[2]~input_o\)))) # (!\R[4]~19_combout\ & (\E[4][4]~input_o\ & ((\S[2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[4]~19_combout\,
	datab => \E[4][4]~input_o\,
	datac => \E[5][4]~input_o\,
	datad => \S[2]~input_o\,
	combout => \R[4]~20_combout\);

-- Location: IOIBUF_X78_Y42_N22
\E[2][4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(4),
	o => \E[2][4]~input_o\);

-- Location: LCCOMB_X75_Y42_N0
\R[4]~21\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[4]~21_combout\ = (\R[0]~0_combout\ & (((\R[0]~3_combout\)))) # (!\R[0]~0_combout\ & ((\R[0]~3_combout\ & (\R[4]~20_combout\)) # (!\R[0]~3_combout\ & ((\E[2][4]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[0]~0_combout\,
	datab => \R[4]~20_combout\,
	datac => \E[2][4]~input_o\,
	datad => \R[0]~3_combout\,
	combout => \R[4]~21_combout\);

-- Location: IOIBUF_X60_Y54_N1
\E[3][4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(4),
	o => \E[3][4]~input_o\);

-- Location: LCCOMB_X75_Y42_N22
\R[4]~22\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[4]~22_combout\ = (\R[4]~21_combout\ & ((\E[6][4]~input_o\) # ((!\R[0]~0_combout\)))) # (!\R[4]~21_combout\ & (((\R[0]~0_combout\ & \E[3][4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[6][4]~input_o\,
	datab => \R[4]~21_combout\,
	datac => \R[0]~0_combout\,
	datad => \E[3][4]~input_o\,
	combout => \R[4]~22_combout\);

-- Location: LCCOMB_X75_Y42_N8
\R[4]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[4]$latch~combout\ = (GLOBAL(\R[15]~6clkctrl_outclk\) & (\R[4]~22_combout\)) # (!GLOBAL(\R[15]~6clkctrl_outclk\) & ((\R[4]$latch~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[4]~22_combout\,
	datac => \R[4]$latch~combout\,
	datad => \R[15]~6clkctrl_outclk\,
	combout => \R[4]$latch~combout\);

-- Location: IOIBUF_X78_Y43_N1
\E[3][5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(5),
	o => \E[3][5]~input_o\);

-- Location: IOIBUF_X34_Y39_N15
\E[2][5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(5),
	o => \E[2][5]~input_o\);

-- Location: LCCOMB_X76_Y42_N0
\R[5]~25\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[5]~25_combout\ = (\R[0]~3_combout\ & (((\R[0]~0_combout\)))) # (!\R[0]~3_combout\ & ((\R[0]~0_combout\ & (\E[3][5]~input_o\)) # (!\R[0]~0_combout\ & ((\E[2][5]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[0]~3_combout\,
	datab => \E[3][5]~input_o\,
	datac => \E[2][5]~input_o\,
	datad => \R[0]~0_combout\,
	combout => \R[5]~25_combout\);

-- Location: IOIBUF_X78_Y37_N8
\E[4][5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(5),
	o => \E[4][5]~input_o\);

-- Location: IOIBUF_X69_Y54_N29
\E[0][5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(5),
	o => \E[0][5]~input_o\);

-- Location: LCCOMB_X76_Y42_N12
\R[5]~23\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[5]~23_combout\ = (\S[0]~input_o\ & (\S[2]~input_o\)) # (!\S[0]~input_o\ & ((\S[2]~input_o\ & (\E[4][5]~input_o\)) # (!\S[2]~input_o\ & ((\E[0][5]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \S[2]~input_o\,
	datac => \E[4][5]~input_o\,
	datad => \E[0][5]~input_o\,
	combout => \R[5]~23_combout\);

-- Location: IOIBUF_X54_Y54_N29
\E[1][5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(5),
	o => \E[1][5]~input_o\);

-- Location: IOIBUF_X36_Y39_N15
\E[5][5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(5),
	o => \E[5][5]~input_o\);

-- Location: LCCOMB_X76_Y42_N26
\R[5]~24\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[5]~24_combout\ = (\R[5]~23_combout\ & (((\E[5][5]~input_o\) # (!\S[0]~input_o\)))) # (!\R[5]~23_combout\ & (\E[1][5]~input_o\ & (\S[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[5]~23_combout\,
	datab => \E[1][5]~input_o\,
	datac => \S[0]~input_o\,
	datad => \E[5][5]~input_o\,
	combout => \R[5]~24_combout\);

-- Location: IOIBUF_X78_Y43_N22
\E[6][5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(5),
	o => \E[6][5]~input_o\);

-- Location: LCCOMB_X76_Y42_N30
\R[5]~26\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[5]~26_combout\ = (\R[0]~3_combout\ & ((\R[5]~25_combout\ & ((\E[6][5]~input_o\))) # (!\R[5]~25_combout\ & (\R[5]~24_combout\)))) # (!\R[0]~3_combout\ & (\R[5]~25_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[0]~3_combout\,
	datab => \R[5]~25_combout\,
	datac => \R[5]~24_combout\,
	datad => \E[6][5]~input_o\,
	combout => \R[5]~26_combout\);

-- Location: LCCOMB_X76_Y42_N18
\R[5]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[5]$latch~combout\ = (GLOBAL(\R[15]~6clkctrl_outclk\) & ((\R[5]~26_combout\))) # (!GLOBAL(\R[15]~6clkctrl_outclk\) & (\R[5]$latch~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[15]~6clkctrl_outclk\,
	datab => \R[5]$latch~combout\,
	datac => \R[5]~26_combout\,
	combout => \R[5]$latch~combout\);

-- Location: IOIBUF_X78_Y35_N1
\E[3][6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(6),
	o => \E[3][6]~input_o\);

-- Location: IOIBUF_X78_Y45_N8
\E[1][6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(6),
	o => \E[1][6]~input_o\);

-- Location: IOIBUF_X69_Y54_N22
\E[0][6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(6),
	o => \E[0][6]~input_o\);

-- Location: LCCOMB_X76_Y42_N28
\R[6]~27\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[6]~27_combout\ = (\S[0]~input_o\ & ((\S[2]~input_o\) # ((\E[1][6]~input_o\)))) # (!\S[0]~input_o\ & (!\S[2]~input_o\ & ((\E[0][6]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \S[2]~input_o\,
	datac => \E[1][6]~input_o\,
	datad => \E[0][6]~input_o\,
	combout => \R[6]~27_combout\);

-- Location: IOIBUF_X78_Y43_N8
\E[5][6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(6),
	o => \E[5][6]~input_o\);

-- Location: IOIBUF_X78_Y45_N22
\E[4][6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(6),
	o => \E[4][6]~input_o\);

-- Location: LCCOMB_X76_Y42_N10
\R[6]~28\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[6]~28_combout\ = (\R[6]~27_combout\ & ((\E[5][6]~input_o\) # ((!\S[2]~input_o\)))) # (!\R[6]~27_combout\ & (((\E[4][6]~input_o\ & \S[2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[6]~27_combout\,
	datab => \E[5][6]~input_o\,
	datac => \E[4][6]~input_o\,
	datad => \S[2]~input_o\,
	combout => \R[6]~28_combout\);

-- Location: IOIBUF_X71_Y54_N29
\E[2][6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(6),
	o => \E[2][6]~input_o\);

-- Location: LCCOMB_X76_Y42_N16
\R[6]~29\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[6]~29_combout\ = (\R[0]~0_combout\ & (((\R[0]~3_combout\)))) # (!\R[0]~0_combout\ & ((\R[0]~3_combout\ & (\R[6]~28_combout\)) # (!\R[0]~3_combout\ & ((\E[2][6]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[6]~28_combout\,
	datab => \E[2][6]~input_o\,
	datac => \R[0]~0_combout\,
	datad => \R[0]~3_combout\,
	combout => \R[6]~29_combout\);

-- Location: IOIBUF_X78_Y43_N15
\E[6][6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(6),
	o => \E[6][6]~input_o\);

-- Location: LCCOMB_X76_Y42_N6
\R[6]~30\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[6]~30_combout\ = (\R[6]~29_combout\ & (((\E[6][6]~input_o\) # (!\R[0]~0_combout\)))) # (!\R[6]~29_combout\ & (\E[3][6]~input_o\ & (\R[0]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110000101100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[3][6]~input_o\,
	datab => \R[6]~29_combout\,
	datac => \R[0]~0_combout\,
	datad => \E[6][6]~input_o\,
	combout => \R[6]~30_combout\);

-- Location: LCCOMB_X76_Y42_N8
\R[6]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[6]$latch~combout\ = (GLOBAL(\R[15]~6clkctrl_outclk\) & (\R[6]~30_combout\)) # (!GLOBAL(\R[15]~6clkctrl_outclk\) & ((\R[6]$latch~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[15]~6clkctrl_outclk\,
	datab => \R[6]~30_combout\,
	datac => \R[6]$latch~combout\,
	combout => \R[6]$latch~combout\);

-- Location: IOIBUF_X78_Y45_N1
\E[6][7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(7),
	o => \E[6][7]~input_o\);

-- Location: IOIBUF_X69_Y54_N8
\E[3][7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(7),
	o => \E[3][7]~input_o\);

-- Location: IOIBUF_X54_Y54_N22
\E[2][7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(7),
	o => \E[2][7]~input_o\);

-- Location: LCCOMB_X76_Y42_N4
\R[7]~33\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[7]~33_combout\ = (\R[0]~3_combout\ & (((\R[0]~0_combout\)))) # (!\R[0]~3_combout\ & ((\R[0]~0_combout\ & (\E[3][7]~input_o\)) # (!\R[0]~0_combout\ & ((\E[2][7]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[0]~3_combout\,
	datab => \E[3][7]~input_o\,
	datac => \E[2][7]~input_o\,
	datad => \R[0]~0_combout\,
	combout => \R[7]~33_combout\);

-- Location: IOIBUF_X78_Y44_N23
\E[5][7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(7),
	o => \E[5][7]~input_o\);

-- Location: IOIBUF_X78_Y45_N15
\E[4][7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(7),
	o => \E[4][7]~input_o\);

-- Location: IOIBUF_X78_Y49_N8
\E[0][7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(7),
	o => \E[0][7]~input_o\);

-- Location: LCCOMB_X76_Y42_N24
\R[7]~31\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[7]~31_combout\ = (\S[0]~input_o\ & (\S[2]~input_o\)) # (!\S[0]~input_o\ & ((\S[2]~input_o\ & (\E[4][7]~input_o\)) # (!\S[2]~input_o\ & ((\E[0][7]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \S[2]~input_o\,
	datac => \E[4][7]~input_o\,
	datad => \E[0][7]~input_o\,
	combout => \R[7]~31_combout\);

-- Location: IOIBUF_X49_Y54_N8
\E[1][7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(7),
	o => \E[1][7]~input_o\);

-- Location: LCCOMB_X76_Y42_N14
\R[7]~32\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[7]~32_combout\ = (\R[7]~31_combout\ & ((\E[5][7]~input_o\) # ((!\S[0]~input_o\)))) # (!\R[7]~31_combout\ & (((\S[0]~input_o\ & \E[1][7]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[5][7]~input_o\,
	datab => \R[7]~31_combout\,
	datac => \S[0]~input_o\,
	datad => \E[1][7]~input_o\,
	combout => \R[7]~32_combout\);

-- Location: LCCOMB_X76_Y42_N22
\R[7]~34\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[7]~34_combout\ = (\R[0]~3_combout\ & ((\R[7]~33_combout\ & (\E[6][7]~input_o\)) # (!\R[7]~33_combout\ & ((\R[7]~32_combout\))))) # (!\R[0]~3_combout\ & (((\R[7]~33_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[0]~3_combout\,
	datab => \E[6][7]~input_o\,
	datac => \R[7]~33_combout\,
	datad => \R[7]~32_combout\,
	combout => \R[7]~34_combout\);

-- Location: LCCOMB_X76_Y42_N2
\R[7]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[7]$latch~combout\ = (GLOBAL(\R[15]~6clkctrl_outclk\) & ((\R[7]~34_combout\))) # (!GLOBAL(\R[15]~6clkctrl_outclk\) & (\R[7]$latch~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010011100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[15]~6clkctrl_outclk\,
	datab => \R[7]$latch~combout\,
	datac => \R[7]~34_combout\,
	combout => \R[7]$latch~combout\);

-- Location: IOIBUF_X36_Y39_N29
\E[1][8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(8),
	o => \E[1][8]~input_o\);

-- Location: IOIBUF_X46_Y54_N15
\E[0][8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(8),
	o => \E[0][8]~input_o\);

-- Location: LCCOMB_X74_Y42_N28
\R[8]~35\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[8]~35_combout\ = (\S[2]~input_o\ & (((\S[0]~input_o\)))) # (!\S[2]~input_o\ & ((\S[0]~input_o\ & (\E[1][8]~input_o\)) # (!\S[0]~input_o\ & ((\E[0][8]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010111100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[2]~input_o\,
	datab => \E[1][8]~input_o\,
	datac => \S[0]~input_o\,
	datad => \E[0][8]~input_o\,
	combout => \R[8]~35_combout\);

-- Location: IOIBUF_X46_Y54_N22
\E[5][8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(8),
	o => \E[5][8]~input_o\);

-- Location: IOIBUF_X54_Y54_N1
\E[4][8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(8),
	o => \E[4][8]~input_o\);

-- Location: LCCOMB_X74_Y42_N6
\R[8]~36\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[8]~36_combout\ = (\R[8]~35_combout\ & ((\E[5][8]~input_o\) # ((!\S[2]~input_o\)))) # (!\R[8]~35_combout\ & (((\S[2]~input_o\ & \E[4][8]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[8]~35_combout\,
	datab => \E[5][8]~input_o\,
	datac => \S[2]~input_o\,
	datad => \E[4][8]~input_o\,
	combout => \R[8]~36_combout\);

-- Location: IOIBUF_X58_Y54_N29
\E[2][8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(8),
	o => \E[2][8]~input_o\);

-- Location: LCCOMB_X74_Y42_N4
\R[8]~37\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[8]~37_combout\ = (\R[0]~0_combout\ & (((\R[0]~3_combout\)))) # (!\R[0]~0_combout\ & ((\R[0]~3_combout\ & (\R[8]~36_combout\)) # (!\R[0]~3_combout\ & ((\E[2][8]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[8]~36_combout\,
	datab => \R[0]~0_combout\,
	datac => \E[2][8]~input_o\,
	datad => \R[0]~3_combout\,
	combout => \R[8]~37_combout\);

-- Location: IOIBUF_X66_Y54_N8
\E[3][8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(8),
	o => \E[3][8]~input_o\);

-- Location: IOIBUF_X46_Y54_N29
\E[6][8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(8),
	o => \E[6][8]~input_o\);

-- Location: LCCOMB_X74_Y42_N30
\R[8]~38\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[8]~38_combout\ = (\R[8]~37_combout\ & (((\E[6][8]~input_o\) # (!\R[0]~0_combout\)))) # (!\R[8]~37_combout\ & (\E[3][8]~input_o\ & (\R[0]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[8]~37_combout\,
	datab => \E[3][8]~input_o\,
	datac => \R[0]~0_combout\,
	datad => \E[6][8]~input_o\,
	combout => \R[8]~38_combout\);

-- Location: LCCOMB_X74_Y42_N12
\R[8]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[8]$latch~combout\ = (GLOBAL(\R[15]~6clkctrl_outclk\) & ((\R[8]~38_combout\))) # (!GLOBAL(\R[15]~6clkctrl_outclk\) & (\R[8]$latch~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[8]$latch~combout\,
	datab => \R[8]~38_combout\,
	datad => \R[15]~6clkctrl_outclk\,
	combout => \R[8]$latch~combout\);

-- Location: IOIBUF_X78_Y41_N8
\E[6][9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(9),
	o => \E[6][9]~input_o\);

-- Location: IOIBUF_X78_Y41_N23
\E[3][9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(9),
	o => \E[3][9]~input_o\);

-- Location: IOIBUF_X34_Y39_N8
\E[2][9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(9),
	o => \E[2][9]~input_o\);

-- Location: LCCOMB_X77_Y41_N16
\R[9]~41\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[9]~41_combout\ = (\R[0]~0_combout\ & ((\E[3][9]~input_o\) # ((\R[0]~3_combout\)))) # (!\R[0]~0_combout\ & (((\E[2][9]~input_o\ & !\R[0]~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[3][9]~input_o\,
	datab => \R[0]~0_combout\,
	datac => \E[2][9]~input_o\,
	datad => \R[0]~3_combout\,
	combout => \R[9]~41_combout\);

-- Location: IOIBUF_X78_Y41_N15
\E[1][9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(9),
	o => \E[1][9]~input_o\);

-- Location: IOIBUF_X78_Y30_N8
\E[5][9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(9),
	o => \E[5][9]~input_o\);

-- Location: IOIBUF_X49_Y54_N29
\E[4][9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(9),
	o => \E[4][9]~input_o\);

-- Location: IOIBUF_X78_Y37_N15
\E[0][9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(9),
	o => \E[0][9]~input_o\);

-- Location: LCCOMB_X77_Y41_N0
\R[9]~39\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[9]~39_combout\ = (\S[2]~input_o\ & ((\E[4][9]~input_o\) # ((\S[0]~input_o\)))) # (!\S[2]~input_o\ & (((!\S[0]~input_o\ & \E[0][9]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[4][9]~input_o\,
	datab => \S[2]~input_o\,
	datac => \S[0]~input_o\,
	datad => \E[0][9]~input_o\,
	combout => \R[9]~39_combout\);

-- Location: LCCOMB_X77_Y41_N26
\R[9]~40\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[9]~40_combout\ = (\R[9]~39_combout\ & (((\E[5][9]~input_o\) # (!\S[0]~input_o\)))) # (!\R[9]~39_combout\ & (\E[1][9]~input_o\ & ((\S[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[1][9]~input_o\,
	datab => \E[5][9]~input_o\,
	datac => \R[9]~39_combout\,
	datad => \S[0]~input_o\,
	combout => \R[9]~40_combout\);

-- Location: LCCOMB_X77_Y41_N14
\R[9]~42\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[9]~42_combout\ = (\R[9]~41_combout\ & ((\E[6][9]~input_o\) # ((!\R[0]~3_combout\)))) # (!\R[9]~41_combout\ & (((\R[9]~40_combout\ & \R[0]~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[6][9]~input_o\,
	datab => \R[9]~41_combout\,
	datac => \R[9]~40_combout\,
	datad => \R[0]~3_combout\,
	combout => \R[9]~42_combout\);

-- Location: LCCOMB_X77_Y41_N28
\R[9]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[9]$latch~combout\ = (GLOBAL(\R[15]~6clkctrl_outclk\) & ((\R[9]~42_combout\))) # (!GLOBAL(\R[15]~6clkctrl_outclk\) & (\R[9]$latch~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \R[9]$latch~combout\,
	datac => \R[15]~6clkctrl_outclk\,
	datad => \R[9]~42_combout\,
	combout => \R[9]$latch~combout\);

-- Location: IOIBUF_X78_Y44_N15
\E[4][10]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(10),
	o => \E[4][10]~input_o\);

-- Location: IOIBUF_X78_Y23_N8
\E[1][10]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(10),
	o => \E[1][10]~input_o\);

-- Location: IOIBUF_X78_Y34_N15
\E[0][10]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(10),
	o => \E[0][10]~input_o\);

-- Location: LCCOMB_X77_Y42_N30
\R[10]~43\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[10]~43_combout\ = (\S[2]~input_o\ & (((\S[0]~input_o\)))) # (!\S[2]~input_o\ & ((\S[0]~input_o\ & (\E[1][10]~input_o\)) # (!\S[0]~input_o\ & ((\E[0][10]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[1][10]~input_o\,
	datab => \S[2]~input_o\,
	datac => \E[0][10]~input_o\,
	datad => \S[0]~input_o\,
	combout => \R[10]~43_combout\);

-- Location: IOIBUF_X78_Y34_N23
\E[5][10]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(10),
	o => \E[5][10]~input_o\);

-- Location: LCCOMB_X77_Y42_N24
\R[10]~44\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[10]~44_combout\ = (\R[10]~43_combout\ & (((\E[5][10]~input_o\) # (!\S[2]~input_o\)))) # (!\R[10]~43_combout\ & (\E[4][10]~input_o\ & ((\S[2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[4][10]~input_o\,
	datab => \R[10]~43_combout\,
	datac => \E[5][10]~input_o\,
	datad => \S[2]~input_o\,
	combout => \R[10]~44_combout\);

-- Location: IOIBUF_X78_Y35_N22
\E[2][10]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(10),
	o => \E[2][10]~input_o\);

-- Location: LCCOMB_X77_Y42_N6
\R[10]~45\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[10]~45_combout\ = (\R[0]~3_combout\ & ((\R[10]~44_combout\) # ((\R[0]~0_combout\)))) # (!\R[0]~3_combout\ & (((!\R[0]~0_combout\ & \E[2][10]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[10]~44_combout\,
	datab => \R[0]~3_combout\,
	datac => \R[0]~0_combout\,
	datad => \E[2][10]~input_o\,
	combout => \R[10]~45_combout\);

-- Location: IOIBUF_X78_Y31_N1
\E[3][10]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(10),
	o => \E[3][10]~input_o\);

-- Location: IOIBUF_X78_Y37_N22
\E[6][10]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(10),
	o => \E[6][10]~input_o\);

-- Location: LCCOMB_X77_Y42_N0
\R[10]~46\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[10]~46_combout\ = (\R[10]~45_combout\ & (((\E[6][10]~input_o\) # (!\R[0]~0_combout\)))) # (!\R[10]~45_combout\ & (\E[3][10]~input_o\ & (\R[0]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[10]~45_combout\,
	datab => \E[3][10]~input_o\,
	datac => \R[0]~0_combout\,
	datad => \E[6][10]~input_o\,
	combout => \R[10]~46_combout\);

-- Location: LCCOMB_X77_Y42_N14
\R[10]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[10]$latch~combout\ = (GLOBAL(\R[15]~6clkctrl_outclk\) & ((\R[10]~46_combout\))) # (!GLOBAL(\R[15]~6clkctrl_outclk\) & (\R[10]$latch~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \R[10]$latch~combout\,
	datac => \R[10]~46_combout\,
	datad => \R[15]~6clkctrl_outclk\,
	combout => \R[10]$latch~combout\);

-- Location: IOIBUF_X78_Y35_N15
\E[6][11]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(11),
	o => \E[6][11]~input_o\);

-- Location: IOIBUF_X78_Y49_N15
\E[2][11]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(11),
	o => \E[2][11]~input_o\);

-- Location: IOIBUF_X78_Y49_N22
\E[3][11]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(11),
	o => \E[3][11]~input_o\);

-- Location: LCCOMB_X77_Y42_N8
\R[11]~49\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[11]~49_combout\ = (\R[0]~0_combout\ & (((\R[0]~3_combout\) # (\E[3][11]~input_o\)))) # (!\R[0]~0_combout\ & (\E[2][11]~input_o\ & (!\R[0]~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[2][11]~input_o\,
	datab => \R[0]~0_combout\,
	datac => \R[0]~3_combout\,
	datad => \E[3][11]~input_o\,
	combout => \R[11]~49_combout\);

-- Location: IOIBUF_X78_Y31_N15
\E[1][11]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(11),
	o => \E[1][11]~input_o\);

-- Location: IOIBUF_X78_Y35_N8
\E[5][11]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(11),
	o => \E[5][11]~input_o\);

-- Location: IOIBUF_X78_Y42_N15
\E[4][11]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(11),
	o => \E[4][11]~input_o\);

-- Location: IOIBUF_X78_Y34_N8
\E[0][11]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(11),
	o => \E[0][11]~input_o\);

-- Location: LCCOMB_X76_Y42_N20
\R[11]~47\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[11]~47_combout\ = (\S[0]~input_o\ & (((\S[2]~input_o\)))) # (!\S[0]~input_o\ & ((\S[2]~input_o\ & (\E[4][11]~input_o\)) # (!\S[2]~input_o\ & ((\E[0][11]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \E[4][11]~input_o\,
	datac => \E[0][11]~input_o\,
	datad => \S[2]~input_o\,
	combout => \R[11]~47_combout\);

-- Location: LCCOMB_X77_Y42_N10
\R[11]~48\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[11]~48_combout\ = (\S[0]~input_o\ & ((\R[11]~47_combout\ & ((\E[5][11]~input_o\))) # (!\R[11]~47_combout\ & (\E[1][11]~input_o\)))) # (!\S[0]~input_o\ & (((\R[11]~47_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \E[1][11]~input_o\,
	datac => \E[5][11]~input_o\,
	datad => \R[11]~47_combout\,
	combout => \R[11]~48_combout\);

-- Location: LCCOMB_X77_Y42_N2
\R[11]~50\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[11]~50_combout\ = (\R[11]~49_combout\ & ((\E[6][11]~input_o\) # ((!\R[0]~3_combout\)))) # (!\R[11]~49_combout\ & (((\R[0]~3_combout\ & \R[11]~48_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[6][11]~input_o\,
	datab => \R[11]~49_combout\,
	datac => \R[0]~3_combout\,
	datad => \R[11]~48_combout\,
	combout => \R[11]~50_combout\);

-- Location: LCCOMB_X77_Y42_N16
\R[11]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[11]$latch~combout\ = (GLOBAL(\R[15]~6clkctrl_outclk\) & ((\R[11]~50_combout\))) # (!GLOBAL(\R[15]~6clkctrl_outclk\) & (\R[11]$latch~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \R[11]$latch~combout\,
	datac => \R[11]~50_combout\,
	datad => \R[15]~6clkctrl_outclk\,
	combout => \R[11]$latch~combout\);

-- Location: IOIBUF_X49_Y54_N15
\E[5][12]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(12),
	o => \E[5][12]~input_o\);

-- Location: IOIBUF_X78_Y30_N1
\E[1][12]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(12),
	o => \E[1][12]~input_o\);

-- Location: IOIBUF_X78_Y44_N8
\E[0][12]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(12),
	o => \E[0][12]~input_o\);

-- Location: LCCOMB_X77_Y42_N28
\R[12]~51\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[12]~51_combout\ = (\S[2]~input_o\ & (((\S[0]~input_o\)))) # (!\S[2]~input_o\ & ((\S[0]~input_o\ & (\E[1][12]~input_o\)) # (!\S[0]~input_o\ & ((\E[0][12]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[1][12]~input_o\,
	datab => \S[2]~input_o\,
	datac => \E[0][12]~input_o\,
	datad => \S[0]~input_o\,
	combout => \R[12]~51_combout\);

-- Location: IOIBUF_X78_Y30_N22
\E[4][12]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(12),
	o => \E[4][12]~input_o\);

-- Location: LCCOMB_X77_Y42_N18
\R[12]~52\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[12]~52_combout\ = (\R[12]~51_combout\ & ((\E[5][12]~input_o\) # ((!\S[2]~input_o\)))) # (!\R[12]~51_combout\ & (((\E[4][12]~input_o\ & \S[2]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[5][12]~input_o\,
	datab => \R[12]~51_combout\,
	datac => \E[4][12]~input_o\,
	datad => \S[2]~input_o\,
	combout => \R[12]~52_combout\);

-- Location: IOIBUF_X78_Y44_N1
\E[2][12]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(12),
	o => \E[2][12]~input_o\);

-- Location: LCCOMB_X77_Y42_N12
\R[12]~53\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[12]~53_combout\ = (\R[0]~3_combout\ & ((\R[12]~52_combout\) # ((\R[0]~0_combout\)))) # (!\R[0]~3_combout\ & (((!\R[0]~0_combout\ & \E[2][12]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101111001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[12]~52_combout\,
	datab => \R[0]~3_combout\,
	datac => \R[0]~0_combout\,
	datad => \E[2][12]~input_o\,
	combout => \R[12]~53_combout\);

-- Location: IOIBUF_X78_Y34_N1
\E[3][12]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(12),
	o => \E[3][12]~input_o\);

-- Location: IOIBUF_X78_Y25_N1
\E[6][12]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(12),
	o => \E[6][12]~input_o\);

-- Location: LCCOMB_X77_Y42_N22
\R[12]~54\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[12]~54_combout\ = (\R[12]~53_combout\ & (((\E[6][12]~input_o\) # (!\R[0]~0_combout\)))) # (!\R[12]~53_combout\ & (\E[3][12]~input_o\ & (\R[0]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[12]~53_combout\,
	datab => \E[3][12]~input_o\,
	datac => \R[0]~0_combout\,
	datad => \E[6][12]~input_o\,
	combout => \R[12]~54_combout\);

-- Location: LCCOMB_X77_Y42_N26
\R[12]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[12]$latch~combout\ = (GLOBAL(\R[15]~6clkctrl_outclk\) & (\R[12]~54_combout\)) # (!GLOBAL(\R[15]~6clkctrl_outclk\) & ((\R[12]$latch~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \R[12]~54_combout\,
	datac => \R[12]$latch~combout\,
	datad => \R[15]~6clkctrl_outclk\,
	combout => \R[12]$latch~combout\);

-- Location: IOIBUF_X78_Y25_N15
\E[3][13]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(13),
	o => \E[3][13]~input_o\);

-- Location: IOIBUF_X78_Y29_N8
\E[2][13]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(13),
	o => \E[2][13]~input_o\);

-- Location: LCCOMB_X77_Y42_N4
\R[13]~57\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[13]~57_combout\ = (\R[0]~3_combout\ & (((\R[0]~0_combout\)))) # (!\R[0]~3_combout\ & ((\R[0]~0_combout\ & (\E[3][13]~input_o\)) # (!\R[0]~0_combout\ & ((\E[2][13]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[3][13]~input_o\,
	datab => \R[0]~3_combout\,
	datac => \E[2][13]~input_o\,
	datad => \R[0]~0_combout\,
	combout => \R[13]~57_combout\);

-- Location: IOIBUF_X78_Y29_N15
\E[0][13]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(13),
	o => \E[0][13]~input_o\);

-- Location: IOIBUF_X78_Y36_N23
\E[4][13]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(13),
	o => \E[4][13]~input_o\);

-- Location: LCCOMB_X77_Y40_N20
\R[13]~55\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[13]~55_combout\ = (\S[0]~input_o\ & (((\S[2]~input_o\)))) # (!\S[0]~input_o\ & ((\S[2]~input_o\ & ((\E[4][13]~input_o\))) # (!\S[2]~input_o\ & (\E[0][13]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[0][13]~input_o\,
	datab => \S[0]~input_o\,
	datac => \S[2]~input_o\,
	datad => \E[4][13]~input_o\,
	combout => \R[13]~55_combout\);

-- Location: IOIBUF_X78_Y36_N8
\E[1][13]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(13),
	o => \E[1][13]~input_o\);

-- Location: IOIBUF_X78_Y40_N22
\E[5][13]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(13),
	o => \E[5][13]~input_o\);

-- Location: LCCOMB_X77_Y40_N26
\R[13]~56\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[13]~56_combout\ = (\S[0]~input_o\ & ((\R[13]~55_combout\ & ((\E[5][13]~input_o\))) # (!\R[13]~55_combout\ & (\E[1][13]~input_o\)))) # (!\S[0]~input_o\ & (\R[13]~55_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110001100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \R[13]~55_combout\,
	datac => \E[1][13]~input_o\,
	datad => \E[5][13]~input_o\,
	combout => \R[13]~56_combout\);

-- Location: IOIBUF_X78_Y30_N15
\E[6][13]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(13),
	o => \E[6][13]~input_o\);

-- Location: LCCOMB_X77_Y40_N0
\R[13]~58\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[13]~58_combout\ = (\R[13]~57_combout\ & (((\E[6][13]~input_o\)) # (!\R[0]~3_combout\))) # (!\R[13]~57_combout\ & (\R[0]~3_combout\ & (\R[13]~56_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101001100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[13]~57_combout\,
	datab => \R[0]~3_combout\,
	datac => \R[13]~56_combout\,
	datad => \E[6][13]~input_o\,
	combout => \R[13]~58_combout\);

-- Location: LCCOMB_X77_Y40_N10
\R[13]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[13]$latch~combout\ = (GLOBAL(\R[15]~6clkctrl_outclk\) & (\R[13]~58_combout\)) # (!GLOBAL(\R[15]~6clkctrl_outclk\) & ((\R[13]$latch~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R[15]~6clkctrl_outclk\,
	datac => \R[13]~58_combout\,
	datad => \R[13]$latch~combout\,
	combout => \R[13]$latch~combout\);

-- Location: IOIBUF_X34_Y39_N22
\E[3][14]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(14),
	o => \E[3][14]~input_o\);

-- Location: IOIBUF_X78_Y36_N15
\E[6][14]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(14),
	o => \E[6][14]~input_o\);

-- Location: IOIBUF_X78_Y24_N1
\E[2][14]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(14),
	o => \E[2][14]~input_o\);

-- Location: IOIBUF_X78_Y33_N1
\E[5][14]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(14),
	o => \E[5][14]~input_o\);

-- Location: IOIBUF_X78_Y40_N1
\E[1][14]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(14),
	o => \E[1][14]~input_o\);

-- Location: IOIBUF_X78_Y23_N1
\E[0][14]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(14),
	o => \E[0][14]~input_o\);

-- Location: IOIBUF_X78_Y33_N15
\E[4][14]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(14),
	o => \E[4][14]~input_o\);

-- Location: LCCOMB_X77_Y40_N2
\R[14]~59\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[14]~59_combout\ = (\S[2]~input_o\ & (((\E[4][14]~input_o\) # (\S[0]~input_o\)))) # (!\S[2]~input_o\ & (\E[0][14]~input_o\ & ((!\S[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[0][14]~input_o\,
	datab => \E[4][14]~input_o\,
	datac => \S[2]~input_o\,
	datad => \S[0]~input_o\,
	combout => \R[14]~59_combout\);

-- Location: LCCOMB_X77_Y40_N16
\R[14]~60\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[14]~60_combout\ = (\R[14]~59_combout\ & ((\E[5][14]~input_o\) # ((!\S[0]~input_o\)))) # (!\R[14]~59_combout\ & (((\E[1][14]~input_o\ & \S[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[5][14]~input_o\,
	datab => \E[1][14]~input_o\,
	datac => \R[14]~59_combout\,
	datad => \S[0]~input_o\,
	combout => \R[14]~60_combout\);

-- Location: LCCOMB_X77_Y40_N22
\R[14]~61\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[14]~61_combout\ = (\R[0]~0_combout\ & (((\R[0]~3_combout\)))) # (!\R[0]~0_combout\ & ((\R[0]~3_combout\ & ((\R[14]~60_combout\))) # (!\R[0]~3_combout\ & (\E[2][14]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[2][14]~input_o\,
	datab => \R[0]~0_combout\,
	datac => \R[14]~60_combout\,
	datad => \R[0]~3_combout\,
	combout => \R[14]~61_combout\);

-- Location: LCCOMB_X77_Y40_N8
\R[14]~62\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[14]~62_combout\ = (\R[14]~61_combout\ & (((\E[6][14]~input_o\) # (!\R[0]~0_combout\)))) # (!\R[14]~61_combout\ & (\E[3][14]~input_o\ & ((\R[0]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[3][14]~input_o\,
	datab => \E[6][14]~input_o\,
	datac => \R[14]~61_combout\,
	datad => \R[0]~0_combout\,
	combout => \R[14]~62_combout\);

-- Location: LCCOMB_X77_Y40_N28
\R[14]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[14]$latch~combout\ = (GLOBAL(\R[15]~6clkctrl_outclk\) & (\R[14]~62_combout\)) # (!GLOBAL(\R[15]~6clkctrl_outclk\) & ((\R[14]$latch~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \R[14]~62_combout\,
	datac => \R[15]~6clkctrl_outclk\,
	datad => \R[14]$latch~combout\,
	combout => \R[14]$latch~combout\);

-- Location: IOIBUF_X78_Y33_N8
\E[6][15]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(6)(15),
	o => \E[6][15]~input_o\);

-- Location: IOIBUF_X78_Y40_N15
\E[5][15]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(5)(15),
	o => \E[5][15]~input_o\);

-- Location: IOIBUF_X78_Y36_N1
\E[0][15]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(0)(15),
	o => \E[0][15]~input_o\);

-- Location: IOIBUF_X34_Y39_N29
\E[4][15]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(4)(15),
	o => \E[4][15]~input_o\);

-- Location: LCCOMB_X77_Y40_N30
\R[15]~63\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[15]~63_combout\ = (\S[0]~input_o\ & (((\S[2]~input_o\)))) # (!\S[0]~input_o\ & ((\S[2]~input_o\ & ((\E[4][15]~input_o\))) # (!\S[2]~input_o\ & (\E[0][15]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001011000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[0][15]~input_o\,
	datab => \S[0]~input_o\,
	datac => \S[2]~input_o\,
	datad => \E[4][15]~input_o\,
	combout => \R[15]~63_combout\);

-- Location: IOIBUF_X78_Y40_N8
\E[1][15]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(1)(15),
	o => \E[1][15]~input_o\);

-- Location: LCCOMB_X77_Y40_N12
\R[15]~64\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[15]~64_combout\ = (\S[0]~input_o\ & ((\R[15]~63_combout\ & (\E[5][15]~input_o\)) # (!\R[15]~63_combout\ & ((\E[1][15]~input_o\))))) # (!\S[0]~input_o\ & (((\R[15]~63_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101101011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S[0]~input_o\,
	datab => \E[5][15]~input_o\,
	datac => \R[15]~63_combout\,
	datad => \E[1][15]~input_o\,
	combout => \R[15]~64_combout\);

-- Location: IOIBUF_X78_Y29_N1
\E[2][15]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(2)(15),
	o => \E[2][15]~input_o\);

-- Location: IOIBUF_X78_Y31_N22
\E[3][15]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E(3)(15),
	o => \E[3][15]~input_o\);

-- Location: LCCOMB_X77_Y40_N18
\R[15]~65\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[15]~65_combout\ = (\R[0]~3_combout\ & (((\R[0]~0_combout\)))) # (!\R[0]~3_combout\ & ((\R[0]~0_combout\ & ((\E[3][15]~input_o\))) # (!\R[0]~0_combout\ & (\E[2][15]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[2][15]~input_o\,
	datab => \R[0]~3_combout\,
	datac => \E[3][15]~input_o\,
	datad => \R[0]~0_combout\,
	combout => \R[15]~65_combout\);

-- Location: LCCOMB_X77_Y40_N4
\R[15]~66\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[15]~66_combout\ = (\R[0]~3_combout\ & ((\R[15]~65_combout\ & (\E[6][15]~input_o\)) # (!\R[15]~65_combout\ & ((\R[15]~64_combout\))))) # (!\R[0]~3_combout\ & (((\R[15]~65_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \E[6][15]~input_o\,
	datab => \R[0]~3_combout\,
	datac => \R[15]~64_combout\,
	datad => \R[15]~65_combout\,
	combout => \R[15]~66_combout\);

-- Location: LCCOMB_X77_Y40_N14
\R[15]$latch\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \R[15]$latch~combout\ = (GLOBAL(\R[15]~6clkctrl_outclk\) & ((\R[15]~66_combout\))) # (!GLOBAL(\R[15]~6clkctrl_outclk\) & (\R[15]$latch~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \R[15]$latch~combout\,
	datac => \R[15]~6clkctrl_outclk\,
	datad => \R[15]~66_combout\,
	combout => \R[15]$latch~combout\);

-- Location: UNVM_X0_Y40_N40
\~QUARTUS_CREATED_UNVM~\ : fiftyfivenm_unvm
-- pragma translate_off
GENERIC MAP (
	addr_range1_end_addr => -1,
	addr_range1_offset => -1,
	addr_range2_end_addr => -1,
	addr_range2_offset => -1,
	addr_range3_offset => -1,
	is_compressed_image => "false",
	is_dual_boot => "false",
	is_eram_skip => "false",
	max_ufm_valid_addr => -1,
	max_valid_addr => -1,
	min_ufm_valid_addr => -1,
	min_valid_addr => -1,
	part_name => "quartus_created_unvm",
	reserve_block => "true")
-- pragma translate_on
PORT MAP (
	nosc_ena => \~QUARTUS_CREATED_GND~I_combout\,
	xe_ye => \~QUARTUS_CREATED_GND~I_combout\,
	se => \~QUARTUS_CREATED_GND~I_combout\,
	busy => \~QUARTUS_CREATED_UNVM~~busy\);

-- Location: ADCBLOCK_X43_Y52_N0
\~QUARTUS_CREATED_ADC1~\ : fiftyfivenm_adcblock
-- pragma translate_off
GENERIC MAP (
	analog_input_pin_mask => 0,
	clkdiv => 1,
	device_partname_fivechar_prefix => "none",
	is_this_first_or_second_adc => 1,
	prescalar => 0,
	pwd => 1,
	refsel => 0,
	reserve_block => "true",
	testbits => 66,
	tsclkdiv => 1,
	tsclksel => 0)
-- pragma translate_on
PORT MAP (
	soc => \~QUARTUS_CREATED_GND~I_combout\,
	usr_pwd => VCC,
	tsen => \~QUARTUS_CREATED_GND~I_combout\,
	chsel => \~QUARTUS_CREATED_ADC1~_CHSEL_bus\,
	eoc => \~QUARTUS_CREATED_ADC1~~eoc\);

-- Location: ADCBLOCK_X43_Y51_N0
\~QUARTUS_CREATED_ADC2~\ : fiftyfivenm_adcblock
-- pragma translate_off
GENERIC MAP (
	analog_input_pin_mask => 0,
	clkdiv => 1,
	device_partname_fivechar_prefix => "none",
	is_this_first_or_second_adc => 2,
	prescalar => 0,
	pwd => 1,
	refsel => 0,
	reserve_block => "true",
	testbits => 66,
	tsclkdiv => 1,
	tsclksel => 0)
-- pragma translate_on
PORT MAP (
	soc => \~QUARTUS_CREATED_GND~I_combout\,
	usr_pwd => VCC,
	tsen => \~QUARTUS_CREATED_GND~I_combout\,
	chsel => \~QUARTUS_CREATED_ADC2~_CHSEL_bus\,
	eoc => \~QUARTUS_CREATED_ADC2~~eoc\);

ww_R(0) <= \R[0]~output_o\;

ww_R(1) <= \R[1]~output_o\;

ww_R(2) <= \R[2]~output_o\;

ww_R(3) <= \R[3]~output_o\;

ww_R(4) <= \R[4]~output_o\;

ww_R(5) <= \R[5]~output_o\;

ww_R(6) <= \R[6]~output_o\;

ww_R(7) <= \R[7]~output_o\;

ww_R(8) <= \R[8]~output_o\;

ww_R(9) <= \R[9]~output_o\;

ww_R(10) <= \R[10]~output_o\;

ww_R(11) <= \R[11]~output_o\;

ww_R(12) <= \R[12]~output_o\;

ww_R(13) <= \R[13]~output_o\;

ww_R(14) <= \R[14]~output_o\;

ww_R(15) <= \R[15]~output_o\;
END structure;



LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY Somador8 IS PORT (
	A1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	A2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	S : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	C : OUT STD_LOGIC
);
END Somador8;

ARCHITECTURE BEHAVIORAL OF Somador8 IS

	COMPONENT MeioSomador IS PORT (
		A1 : IN STD_LOGIC;
		A2 : IN STD_LOGIC;
		S : OUT STD_LOGIC;
		C : OUT STD_LOGIC
	);
	END COMPONENT;

	COMPONENT Somador IS PORT (
		A1 : IN STD_LOGIC;
		A2 : IN STD_LOGIC;
		A3 : IN STD_LOGIC;
		S : OUT STD_LOGIC;
		D : OUT STD_LOGIC
	);
	END COMPONENT;
	
	SIGNAL B : STD_LOGIC_VECTOR(6 DOWNTO 0);
	
BEGIN
	MS1 : MeioSomador PORT MAP (
		A1 => A1(0),
		A2 => A2(0),
		S => S(0),
		C => B(0)
	);
	SomadorX:
		FOR i IN 1 to 6 GENERATE
			SX : Somador PORT MAP (
				A1 => A1(i),
				A2 => A2(i),
				A3 => B(i - 1),
				S => S(i),
				D => B(i)
			);
		END GENERATE SomadorX;
	S7 : Somador PORT MAP (
		A1 => A1(7),
		A2 => A2(7),
		A3 => B(6),
		S => S(7),
		D => C
	);
	
END BEHAVIORAL;
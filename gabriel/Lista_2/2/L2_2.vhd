LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY L2_2 IS
END L2_2;

ARCHITECTURE BEHAVIORAL OF L2_2 IS

	SIGNAL A1 : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL A2 : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL S : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL C : STD_LOGIC;

	COMPONENT Somador8 IS PORT (
		A1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		A2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		S : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		C : OUT STD_LOGIC
	);
	END COMPONENT;


BEGIN

	S1 : Somador8 PORT MAP (
		A1 => A1,
		A2 => A2,
		S => S,
		c => c
	);
	
	PROCESS BEGIN
		A1 <= std_logic_vector(to_unsigned(25, A1'length));
		A2 <= std_logic_vector(to_unsigned(34, A2'length));
		WAIT FOR 20 NS;
		A1 <= std_logic_vector(to_unsigned(159, A1'length));
		A2 <= std_logic_vector(to_unsigned(124, A2'length));
		WAIT FOR 20 NS;
		A1 <= std_logic_vector(to_unsigned(100, A1'length));
		A2 <= std_logic_vector(to_unsigned(57, A2'length));
		WAIT FOR 20 NS;
		A1 <= std_logic_vector(to_unsigned(254, A1'length));
		A2 <= std_logic_vector(to_unsigned(245, A2'length));
		WAIT;
	END PROCESS;
	
END BEHAVIORAL;
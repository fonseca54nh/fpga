library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity and2 is
	port(
		a:in std_logic;
		b:in std_logic;
		c:out std_logic
	    );
end and2;

architecture behavioral of and2 is

begin 
	c <= a and b;
end behavioral;



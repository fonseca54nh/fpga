library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity and2 is
	port(
		a:in std_logic;
		b:in std_logic;
		c:in std_logic;
		d:in std_logic;
		s:out std_logic
	    );
end and2;

architecture behavioral of and2 is

begin 
	-- 1. a
	--s <= not( ( a and not(b) ) or ( not( c and d ) ) );

	-- 1. b
	--s <= not( ( ( ( a and b ) and not(c) ) or ( not( c and d ) ) ) xor ( d ) );
end behavioral;


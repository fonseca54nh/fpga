library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity tb is 
end tb;

architecture test of tb is
	component and2
	port(
		a:in std_logic;
		b:in std_logic;
		c:in std_logic;
		d:in std_logic;
		s:out std_logic
	    );
	end component;

	signal a, b, c, d, s : std_logic;
begin
	portaand2 : and2 port map( a => a, b => b, c=>c, d=>d, s=>s );
	process begin
		a <= '1';
		b <= '1';
		c <= '1';
		d <= '1';
		wait for 1 ns;

		assert false report "Reached end of test";
		wait;
	end process;
end test;


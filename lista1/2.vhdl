library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity ha is
	port(
		a:in std_logic;
		b:in std_logic;
		s:out std_logic;
		c:out std_logic
	    );
end ha;

architecture behavioral of ha is

begin 
	s <= xor( a and b );
	c <= a and b;
end behavioral;

